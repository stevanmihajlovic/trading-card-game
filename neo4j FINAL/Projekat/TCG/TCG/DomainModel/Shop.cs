﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCG.DomainModel
{
    public class Shop
    {
        public int id { get; set; }
        public String name { get; set; }
        public List<MonsterCard> monsterCards { get; set; }
        public List<SpellCard> spellCards { get; set; }
        public List<TrapCard> trapCards { get; set; }
        public float balance { get; set; }

        public Shop()
        {
            monsterCards = new List<MonsterCard>();
            spellCards = new List<SpellCard>();
            trapCards = new List<TrapCard>();
            balance = 0;
        }
    }
}
