﻿namespace TCG.Forms
{
    partial class AddCardsToCollectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSearch = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxToValue = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFromValue = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxRarity = new System.Windows.Forms.ComboBox();
            this.comboBoxCondition = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.groupBoxTrapCard = new System.Windows.Forms.GroupBox();
            this.comboBoxTrapIcon = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxSpellCard = new System.Windows.Forms.GroupBox();
            this.comboBoxSpellIcon = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBoxMonsterCard = new System.Windows.Forms.GroupBox();
            this.textBoxToDefense = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxFromDefense = new System.Windows.Forms.TextBox();
            this.textBoxToAttack = new System.Windows.Forms.TextBox();
            this.comboBoxMonsterLevel = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxMonsterAttribute = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxMonsterType = new System.Windows.Forms.ComboBox();
            this.textBoxFromAttack = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.buttonDone = new System.Windows.Forms.Button();
            this.buttonSearchMyCollection = new System.Windows.Forms.Button();
            this.buttonDeleteCard = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.buttonSearchOtherUsersCollection = new System.Windows.Forms.Button();
            this.labelListBox2 = new System.Windows.Forms.Label();
            this.labelListBox1 = new System.Windows.Forms.Label();
            this.textBoxQuantityOther = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.buttonTrade = new System.Windows.Forms.Button();
            this.buttonSearchShop = new System.Windows.Forms.Button();
            this.buttonTradeShop = new System.Windows.Forms.Button();
            this.textBoxQuantityShop = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBoxTrapCard.SuspendLayout();
            this.groupBoxSpellCard.SuspendLayout();
            this.groupBoxMonsterCard.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(203, 389);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(131, 39);
            this.buttonSearch.TabIndex = 27;
            this.buttonSearch.Text = "Search Entire Database";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxToValue);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBoxType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxFromValue);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.comboBoxRarity);
            this.groupBox1.Controls.Add(this.comboBoxCondition);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Location = new System.Drawing.Point(28, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 184);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Information:";
            // 
            // textBoxToValue
            // 
            this.textBoxToValue.Location = new System.Drawing.Point(246, 141);
            this.textBoxToValue.Name = "textBoxToValue";
            this.textBoxToValue.Size = new System.Drawing.Size(54, 20);
            this.textBoxToValue.TabIndex = 17;
            this.textBoxToValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(220, 148);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "To";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(111, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "From:";
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "MonsterCard",
            "SpellCard",
            "TrapCard"});
            this.comboBoxType.Location = new System.Drawing.Point(112, 34);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(188, 21);
            this.comboBoxType.TabIndex = 14;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Type:";
            // 
            // textBoxFromValue
            // 
            this.textBoxFromValue.Location = new System.Drawing.Point(150, 141);
            this.textBoxFromValue.Name = "textBoxFromValue";
            this.textBoxFromValue.Size = new System.Drawing.Size(48, 20);
            this.textBoxFromValue.TabIndex = 12;
            this.textBoxFromValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Value:";
            // 
            // comboBoxRarity
            // 
            this.comboBoxRarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRarity.FormattingEnabled = true;
            this.comboBoxRarity.Items.AddRange(new object[] {
            "Common",
            "Rare",
            "Super Rare",
            "Ultra Rare",
            "Secret Rare",
            "Ultimate Rare",
            "Gold Rare",
            "Ghost Rare"});
            this.comboBoxRarity.Location = new System.Drawing.Point(112, 113);
            this.comboBoxRarity.Name = "comboBoxRarity";
            this.comboBoxRarity.Size = new System.Drawing.Size(188, 21);
            this.comboBoxRarity.TabIndex = 10;
            // 
            // comboBoxCondition
            // 
            this.comboBoxCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCondition.FormattingEnabled = true;
            this.comboBoxCondition.Items.AddRange(new object[] {
            "Near Mint",
            "Used",
            "Damaged"});
            this.comboBoxCondition.Location = new System.Drawing.Point(112, 86);
            this.comboBoxCondition.Name = "comboBoxCondition";
            this.comboBoxCondition.Size = new System.Drawing.Size(188, 21);
            this.comboBoxCondition.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Rarity: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Condition";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(112, 59);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(188, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // groupBoxTrapCard
            // 
            this.groupBoxTrapCard.Controls.Add(this.comboBoxTrapIcon);
            this.groupBoxTrapCard.Controls.Add(this.label6);
            this.groupBoxTrapCard.Location = new System.Drawing.Point(31, 228);
            this.groupBoxTrapCard.Name = "groupBoxTrapCard";
            this.groupBoxTrapCard.Size = new System.Drawing.Size(303, 44);
            this.groupBoxTrapCard.TabIndex = 25;
            this.groupBoxTrapCard.TabStop = false;
            this.groupBoxTrapCard.Text = "Additional Information:";
            this.groupBoxTrapCard.Visible = false;
            // 
            // comboBoxTrapIcon
            // 
            this.comboBoxTrapIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTrapIcon.FormattingEnabled = true;
            this.comboBoxTrapIcon.Items.AddRange(new object[] {
            "Continous",
            "Counter",
            "Normal"});
            this.comboBoxTrapIcon.Location = new System.Drawing.Point(109, 15);
            this.comboBoxTrapIcon.Name = "comboBoxTrapIcon";
            this.comboBoxTrapIcon.Size = new System.Drawing.Size(188, 21);
            this.comboBoxTrapIcon.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Icon:";
            // 
            // groupBoxSpellCard
            // 
            this.groupBoxSpellCard.Controls.Add(this.comboBoxSpellIcon);
            this.groupBoxSpellCard.Controls.Add(this.label5);
            this.groupBoxSpellCard.Location = new System.Drawing.Point(31, 228);
            this.groupBoxSpellCard.Name = "groupBoxSpellCard";
            this.groupBoxSpellCard.Size = new System.Drawing.Size(303, 44);
            this.groupBoxSpellCard.TabIndex = 24;
            this.groupBoxSpellCard.TabStop = false;
            this.groupBoxSpellCard.Text = "Additional Information:";
            this.groupBoxSpellCard.Visible = false;
            // 
            // comboBoxSpellIcon
            // 
            this.comboBoxSpellIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpellIcon.FormattingEnabled = true;
            this.comboBoxSpellIcon.Items.AddRange(new object[] {
            "Continuous",
            "Equip",
            "Field",
            "Normal",
            "Ritual",
            "Quick Play"});
            this.comboBoxSpellIcon.Location = new System.Drawing.Point(109, 16);
            this.comboBoxSpellIcon.Name = "comboBoxSpellIcon";
            this.comboBoxSpellIcon.Size = new System.Drawing.Size(188, 21);
            this.comboBoxSpellIcon.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Icon:";
            // 
            // groupBoxMonsterCard
            // 
            this.groupBoxMonsterCard.Controls.Add(this.textBoxToDefense);
            this.groupBoxMonsterCard.Controls.Add(this.label17);
            this.groupBoxMonsterCard.Controls.Add(this.label18);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxFromDefense);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxToAttack);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterLevel);
            this.groupBoxMonsterCard.Controls.Add(this.label15);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterAttribute);
            this.groupBoxMonsterCard.Controls.Add(this.label16);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterType);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxFromAttack);
            this.groupBoxMonsterCard.Controls.Add(this.label10);
            this.groupBoxMonsterCard.Controls.Add(this.label9);
            this.groupBoxMonsterCard.Controls.Add(this.label8);
            this.groupBoxMonsterCard.Controls.Add(this.label7);
            this.groupBoxMonsterCard.Controls.Add(this.label4);
            this.groupBoxMonsterCard.Location = new System.Drawing.Point(28, 228);
            this.groupBoxMonsterCard.Name = "groupBoxMonsterCard";
            this.groupBoxMonsterCard.Size = new System.Drawing.Size(306, 150);
            this.groupBoxMonsterCard.TabIndex = 23;
            this.groupBoxMonsterCard.TabStop = false;
            this.groupBoxMonsterCard.Text = "Additional Information:";
            this.groupBoxMonsterCard.Visible = false;
            // 
            // textBoxToDefense
            // 
            this.textBoxToDefense.Location = new System.Drawing.Point(246, 121);
            this.textBoxToDefense.Name = "textBoxToDefense";
            this.textBoxToDefense.Size = new System.Drawing.Size(54, 20);
            this.textBoxToDefense.TabIndex = 25;
            this.textBoxToDefense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(220, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "To";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(111, 128);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "From:";
            // 
            // textBoxFromDefense
            // 
            this.textBoxFromDefense.Location = new System.Drawing.Point(150, 121);
            this.textBoxFromDefense.Name = "textBoxFromDefense";
            this.textBoxFromDefense.Size = new System.Drawing.Size(48, 20);
            this.textBoxFromDefense.TabIndex = 22;
            this.textBoxFromDefense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxToAttack
            // 
            this.textBoxToAttack.Location = new System.Drawing.Point(246, 95);
            this.textBoxToAttack.Name = "textBoxToAttack";
            this.textBoxToAttack.Size = new System.Drawing.Size(54, 20);
            this.textBoxToAttack.TabIndex = 21;
            this.textBoxToAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBoxMonsterLevel
            // 
            this.comboBoxMonsterLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterLevel.FormattingEnabled = true;
            this.comboBoxMonsterLevel.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxMonsterLevel.Location = new System.Drawing.Point(112, 71);
            this.comboBoxMonsterLevel.Name = "comboBoxMonsterLevel";
            this.comboBoxMonsterLevel.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterLevel.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(220, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "To";
            // 
            // comboBoxMonsterAttribute
            // 
            this.comboBoxMonsterAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterAttribute.FormattingEnabled = true;
            this.comboBoxMonsterAttribute.Items.AddRange(new object[] {
            "Dark",
            "Divine",
            "Earth",
            "Fire",
            "Light",
            "Water",
            "Wind"});
            this.comboBoxMonsterAttribute.Location = new System.Drawing.Point(112, 44);
            this.comboBoxMonsterAttribute.Name = "comboBoxMonsterAttribute";
            this.comboBoxMonsterAttribute.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterAttribute.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(111, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "From:";
            // 
            // comboBoxMonsterType
            // 
            this.comboBoxMonsterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterType.FormattingEnabled = true;
            this.comboBoxMonsterType.Items.AddRange(new object[] {
            "Aqua",
            "Beast",
            "Beast-Warrior",
            "Creator God",
            "Dinosaur",
            "Divine-Beast",
            "Dragon",
            "Fairy",
            "Fiend",
            "Fish",
            "Insect",
            "Machine",
            "Plant",
            "Psychic",
            "Pyro",
            "Reptile",
            "Rock",
            "Sea Serpent",
            "Spellcaster",
            "Thunder",
            "Warrior",
            "Winged Beast",
            "Zombie"});
            this.comboBoxMonsterType.Location = new System.Drawing.Point(112, 18);
            this.comboBoxMonsterType.Name = "comboBoxMonsterType";
            this.comboBoxMonsterType.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterType.TabIndex = 9;
            // 
            // textBoxFromAttack
            // 
            this.textBoxFromAttack.Location = new System.Drawing.Point(150, 95);
            this.textBoxFromAttack.Name = "textBoxFromAttack";
            this.textBoxFromAttack.Size = new System.Drawing.Size(48, 20);
            this.textBoxFromAttack.TabIndex = 18;
            this.textBoxFromAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Defense:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Attack:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Level:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Attribute:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Type:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(372, 61);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(212, 316);
            this.listBox1.TabIndex = 22;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(484, 415);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(100, 23);
            this.buttonAdd.TabIndex = 28;
            this.buttonAdd.Text = "Modify Quantity";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Location = new System.Drawing.Point(372, 389);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(49, 13);
            this.labelQuantity.TabIndex = 29;
            this.labelQuantity.Text = "Quantity:";
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Location = new System.Drawing.Point(484, 389);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantity.TabIndex = 30;
            this.textBoxQuantity.Text = "1";
            this.textBoxQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonDone
            // 
            this.buttonDone.Location = new System.Drawing.Point(484, 453);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(100, 23);
            this.buttonDone.TabIndex = 31;
            this.buttonDone.Text = "Done";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // buttonSearchMyCollection
            // 
            this.buttonSearchMyCollection.Location = new System.Drawing.Point(28, 389);
            this.buttonSearchMyCollection.Name = "buttonSearchMyCollection";
            this.buttonSearchMyCollection.Size = new System.Drawing.Size(122, 39);
            this.buttonSearchMyCollection.TabIndex = 32;
            this.buttonSearchMyCollection.Text = "Search My Collection";
            this.buttonSearchMyCollection.UseVisualStyleBackColor = true;
            this.buttonSearchMyCollection.Click += new System.EventHandler(this.buttonSearchMyCollection_Click);
            // 
            // buttonDeleteCard
            // 
            this.buttonDeleteCard.Location = new System.Drawing.Point(372, 415);
            this.buttonDeleteCard.Name = "buttonDeleteCard";
            this.buttonDeleteCard.Size = new System.Drawing.Size(92, 23);
            this.buttonDeleteCard.TabIndex = 33;
            this.buttonDeleteCard.Text = "Delete Card";
            this.buttonDeleteCard.UseVisualStyleBackColor = true;
            this.buttonDeleteCard.Click += new System.EventHandler(this.buttonDeleteCard_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(666, 62);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(212, 316);
            this.listBox2.TabIndex = 34;
            // 
            // buttonSearchOtherUsersCollection
            // 
            this.buttonSearchOtherUsersCollection.Location = new System.Drawing.Point(666, 415);
            this.buttonSearchOtherUsersCollection.Name = "buttonSearchOtherUsersCollection";
            this.buttonSearchOtherUsersCollection.Size = new System.Drawing.Size(103, 42);
            this.buttonSearchOtherUsersCollection.TabIndex = 35;
            this.buttonSearchOtherUsersCollection.Text = "Search Other User\'s Collection";
            this.buttonSearchOtherUsersCollection.UseVisualStyleBackColor = true;
            this.buttonSearchOtherUsersCollection.Click += new System.EventHandler(this.buttonSearchOtherUsersCollection_Click);
            // 
            // labelListBox2
            // 
            this.labelListBox2.AutoSize = true;
            this.labelListBox2.Location = new System.Drawing.Point(663, 26);
            this.labelListBox2.Name = "labelListBox2";
            this.labelListBox2.Size = new System.Drawing.Size(98, 13);
            this.labelListBox2.TabIndex = 36;
            this.labelListBox2.Text = "Other User\'s Cards:";
            // 
            // labelListBox1
            // 
            this.labelListBox1.AutoSize = true;
            this.labelListBox1.Location = new System.Drawing.Point(369, 26);
            this.labelListBox1.Name = "labelListBox1";
            this.labelListBox1.Size = new System.Drawing.Size(0, 13);
            this.labelListBox1.TabIndex = 37;
            // 
            // textBoxQuantityOther
            // 
            this.textBoxQuantityOther.Location = new System.Drawing.Point(775, 389);
            this.textBoxQuantityOther.Name = "textBoxQuantityOther";
            this.textBoxQuantityOther.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantityOther.TabIndex = 39;
            this.textBoxQuantityOther.Text = "1";
            this.textBoxQuantityOther.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(663, 389);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "Quantity:";
            // 
            // buttonTrade
            // 
            this.buttonTrade.Location = new System.Drawing.Point(775, 415);
            this.buttonTrade.Name = "buttonTrade";
            this.buttonTrade.Size = new System.Drawing.Size(100, 42);
            this.buttonTrade.TabIndex = 40;
            this.buttonTrade.Text = "Trade";
            this.buttonTrade.UseVisualStyleBackColor = true;
            this.buttonTrade.Click += new System.EventHandler(this.buttonTrade_Click);
            // 
            // buttonSearchShop
            // 
            this.buttonSearchShop.Location = new System.Drawing.Point(947, 415);
            this.buttonSearchShop.Name = "buttonSearchShop";
            this.buttonSearchShop.Size = new System.Drawing.Size(103, 42);
            this.buttonSearchShop.TabIndex = 41;
            this.buttonSearchShop.Text = "Search Shop";
            this.buttonSearchShop.UseVisualStyleBackColor = true;
            this.buttonSearchShop.Click += new System.EventHandler(this.buttonSearchShop_Click);
            // 
            // buttonTradeShop
            // 
            this.buttonTradeShop.Location = new System.Drawing.Point(1056, 415);
            this.buttonTradeShop.Name = "buttonTradeShop";
            this.buttonTradeShop.Size = new System.Drawing.Size(100, 42);
            this.buttonTradeShop.TabIndex = 45;
            this.buttonTradeShop.Text = "Buy";
            this.buttonTradeShop.UseVisualStyleBackColor = true;
            this.buttonTradeShop.Click += new System.EventHandler(this.buttonTradeShop_Click);
            // 
            // textBoxQuantityShop
            // 
            this.textBoxQuantityShop.Location = new System.Drawing.Point(1056, 389);
            this.textBoxQuantityShop.Name = "textBoxQuantityShop";
            this.textBoxQuantityShop.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantityShop.TabIndex = 44;
            this.textBoxQuantityShop.Text = "1";
            this.textBoxQuantityShop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(944, 389);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "Quantity:";
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(947, 62);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(212, 316);
            this.listBox3.TabIndex = 42;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(944, 26);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(76, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Cards in Shop:";
            // 
            // AddCardsToCollectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 497);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.buttonTradeShop);
            this.Controls.Add(this.textBoxQuantityShop);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.buttonSearchShop);
            this.Controls.Add(this.buttonTrade);
            this.Controls.Add(this.textBoxQuantityOther);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.labelListBox1);
            this.Controls.Add(this.labelListBox2);
            this.Controls.Add(this.buttonSearchOtherUsersCollection);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.buttonDeleteCard);
            this.Controls.Add(this.buttonSearchMyCollection);
            this.Controls.Add(this.buttonDone);
            this.Controls.Add(this.textBoxQuantity);
            this.Controls.Add(this.labelQuantity);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxTrapCard);
            this.Controls.Add(this.groupBoxSpellCard);
            this.Controls.Add(this.groupBoxMonsterCard);
            this.Controls.Add(this.listBox1);
            this.Name = "AddCardsToCollectionForm";
            this.Text = "AddCardsToCollectionForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxTrapCard.ResumeLayout(false);
            this.groupBoxTrapCard.PerformLayout();
            this.groupBoxSpellCard.ResumeLayout(false);
            this.groupBoxSpellCard.PerformLayout();
            this.groupBoxMonsterCard.ResumeLayout(false);
            this.groupBoxMonsterCard.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxToValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFromValue;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxRarity;
        private System.Windows.Forms.ComboBox comboBoxCondition;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.GroupBox groupBoxTrapCard;
        private System.Windows.Forms.ComboBox comboBoxTrapIcon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxSpellCard;
        private System.Windows.Forms.ComboBox comboBoxSpellIcon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBoxMonsterCard;
        private System.Windows.Forms.TextBox textBoxToDefense;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxFromDefense;
        private System.Windows.Forms.TextBox textBoxToAttack;
        private System.Windows.Forms.ComboBox comboBoxMonsterLevel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBoxMonsterAttribute;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBoxMonsterType;
        private System.Windows.Forms.TextBox textBoxFromAttack;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.Button buttonDone;
        private System.Windows.Forms.Button buttonSearchMyCollection;
        private System.Windows.Forms.Button buttonDeleteCard;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button buttonSearchOtherUsersCollection;
        private System.Windows.Forms.Label labelListBox2;
        private System.Windows.Forms.Label labelListBox1;
        private System.Windows.Forms.Button buttonTrade;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxQuantityOther;
        private System.Windows.Forms.Button buttonSearchShop;
        private System.Windows.Forms.Button buttonTradeShop;
        private System.Windows.Forms.TextBox textBoxQuantityShop;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Label label21;
    }
}