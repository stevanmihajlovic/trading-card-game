﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class AddCardsToCollectionForm : Form
    {
        private User user;
        private GraphClient client;
        private string cardType;
        private string queryText;
        private string mode;

        private List<MonsterCard> monsterCards;
        private List<SpellCard> spellCards;
        private List<TrapCard> trapCards;
        private List<int> ids;
        private List<int> quantities;

        private List<MonsterCard> monsterCardsOther;
        private List<SpellCard> spellCardsOther;
        private List<TrapCard> trapCardsOther;
        private List<int> idsOther;
        private List<int> quantitiesOther;
        private List<string> usernameOther;

        private List<MonsterCard> monsterCardsShop;
        private List<SpellCard> spellCardsShop;
        private List<TrapCard> trapCardsShop;
        private List<int> idsShop;
        private List<int> quantitiesShop;

        bool myColl = false;


        public AddCardsToCollectionForm(User u, GraphClient cl)
        {
            InitializeComponent();
            user = u;
            client = cl;
            cardType = "";
            queryText = "";

            ids = new List<int>();
            quantities = new List<int>();
            monsterCards = new List<MonsterCard>();
            spellCards = new List<SpellCard>();
            trapCards = new List<TrapCard>();

            idsOther = new List<int>();
            quantitiesOther = new List<int>();
            monsterCardsOther = new List<MonsterCard>();
            spellCardsOther = new List<SpellCard>();
            trapCardsOther = new List<TrapCard>();
            usernameOther = new List<string>();

            idsShop = new List<int>();
            quantitiesShop = new List<int>();
            monsterCardsShop = new List<MonsterCard>();
            spellCardsShop = new List<SpellCard>();
            trapCardsShop = new List<TrapCard>();
        }
        private void searchQuery(string mode, ListBox lB, List<int> IDs,ref List<MonsterCard> mc, ref List<SpellCard> sc, ref List<TrapCard> tc, List<int> quant, string um)
        {
            quant.Clear();
            lB.Items.Clear();
            IDs.Clear();
            mc.Clear();
            sc.Clear();
            tc.Clear();
            if (um.Equals("<>"))
                usernameOther.Clear();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            if (comboBoxType.SelectedIndex == 0)
            {
                commonQueryProperties(um);
                if (comboBoxMonsterType.SelectedIndex != -1)
                    queryText += " and a.type = '" + comboBoxMonsterType.SelectedItem.ToString() + "'";
                if (comboBoxMonsterAttribute.SelectedIndex != -1)
                    queryText += " and a.attribute = '" + comboBoxMonsterAttribute.SelectedItem.ToString() + "'";
                if (comboBoxMonsterLevel.SelectedIndex != -1)
                    queryText += " and a.level = " + comboBoxMonsterLevel.SelectedItem.ToString();
                int m;

                if (int.TryParse(textBoxFromAttack.Text, out m))
                {
                    textBoxFromAttack.BackColor = Color.White;
                    queryText += " and a.attack >= " + m;
                }

                if (int.TryParse(textBoxToAttack.Text, out m))
                {
                    textBoxToAttack.BackColor = Color.White;
                    queryText += " and a.attack <= " + m;
                }

                if (int.TryParse(textBoxFromDefense.Text, out m))
                {
                    textBoxFromDefense.BackColor = Color.White;
                    queryText += " and a.defense >= " + m;
                }
                
                if (int.TryParse(textBoxToDefense.Text, out m))
                {
                    textBoxToDefense.BackColor = Color.White;
                    queryText += " and a.defense <= " + m;
                }

                if (um.Equals("Shop"))
                    queryText += " and b-[:SELLS]->a return a";
                else
                    queryText += " and " + mode + " b-[:OWNS]->a return distinct a";

                var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                mc = ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();

                foreach (MonsterCard mC in mc)
                {
                    fillQuantities(mC.id, mC.name, mode, lB, IDs, quant, um);
                }
            }
            else
                if (comboBoxType.SelectedIndex == 1)
                {
                    commonQueryProperties(um);
                    if (comboBoxSpellIcon.SelectedIndex != -1)
                        queryText += " and a.icon = '" + comboBoxSpellIcon.SelectedItem.ToString() + "'";

                if (um.Equals("Shop"))
                    queryText += " and b-[:SELLS]->a return a";
                else
                    queryText += " and " + mode + " b-[:OWNS]->a return distinct a";

                var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                    sc = ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();

                    foreach (SpellCard sC in sc)
                    {
                        fillQuantities(sC.id, sC.name, mode, lB, IDs, quant, um);
                    }
                }
                else
                    if (comboBoxType.SelectedIndex == 2)
                    {
                        commonQueryProperties(um);
                        if (comboBoxTrapIcon.SelectedIndex != -1)
                            queryText += " and a.icon = '" + comboBoxTrapIcon.SelectedItem.ToString() + "'";

                        if (um.Equals("Shop"))
                            queryText += " and b-[:SELLS]->a return a";
                        else
                            queryText += " and " + mode + " b-[:OWNS]->a return distinct a";

                        var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                        tc = ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();

                        foreach (TrapCard tC in tc)  
                        {
                            fillQuantities(tC.id, tC.name, mode, lB, IDs, quant, um);
                        }
                    }
                    else
                        MessageBox.Show("Please select type of card first!");
        }
        private void fillQuantities(int id, string name, string mode, ListBox lB, List<int> IDs, List<int> quant, string um)
        {
            if (mode.Equals(""))
                if (um.Equals("<>"))
                {
                    var query = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:User), b-[r:OWNS]->a where b.id <> "
                                                                + user.id + " and a.id = " + id + " return b.username",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                    List<string> names = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).ToList();

                    foreach (string s in names)
                    {
                        IDs.Add(Convert.ToInt32(id));
                        var query1 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:User), b-[r:OWNS]->a where b.username ='"
                                                            + s + "' and a.id = " + id + " return r.quantity",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
                        quant.Add(((IRawGraphClient)client).ExecuteGetCypherResults<int>(query1).ToList().FirstOrDefault());
                        usernameOther.Add(s);
                        lB.Items.Add(quant.Last() + "\t" + name + "\t" + usernameOther.Last());
                    }
                }
                else
                    if (um.Equals("Shop"))
                    {
                        IDs.Add(Convert.ToInt32(id));
                        var query2 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:Shop), b-[r:SELLS]->a where b.id = 1"
                                                                + " and a.id = " + id + " return r.quantity",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                        quant.Add(((IRawGraphClient)client).ExecuteGetCypherResults<int>(query2).ToList().FirstOrDefault());
                        lB.Items.Add(quant.Last() + "\t" + name);
                    }
                    else
                    {
                        IDs.Add(Convert.ToInt32(id));
                        var query2 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:User), b-[r:OWNS]->a where b.id = "
                                                                + user.id + " and a.id = " + id + " return r.quantity",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                        quant.Add(((IRawGraphClient)client).ExecuteGetCypherResults<int>(query2).ToList().FirstOrDefault());
                        lB.Items.Add(quant.Last() + "\t" + name);
                    }
            else
            {
                IDs.Add(Convert.ToInt32(id));
                lB.Items.Add(name);
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            myColl = false;
            labelListBox1.Text = "All Cards";
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            searchQuery("not ", listBox1, ids, ref monsterCards, ref spellCards, ref trapCards, quantities, "=");
            mode = "CREATE";
            buttonAdd.Text = "Add Card";
            buttonDeleteCard.Visible = false;
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();

            if (comboBoxType.SelectedItem.Equals("MonsterCard"))
            {
                cardType = "MonsterCard";
                groupBoxMonsterCard.Visible = true;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("SpellCard"))
            {
                cardType = "SpellCard";
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = true;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("TrapCard"))
            {
                cardType = "TrapCard";
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = true;
            }
        }

        private void commonQueryProperties(string userMode)
        {
            float m;
            if (userMode.Equals("Shop"))
                queryText = "match (a:" + cardType + "), (b:Shop) where b.id = 1";
            else
                queryText = "match (a:" + cardType + "), (b:User) where b.id " + userMode + " " + user.id;
            
            if (textBoxName.Text != "")
                queryText += " and a.name = '" + textBoxName.Text + "'";
            if (comboBoxCondition.SelectedIndex != -1)
                queryText += " and a.condition = '" + comboBoxCondition.SelectedItem.ToString() + "'";
            if (comboBoxRarity.SelectedIndex != -1)
                queryText += " and a.rarity = '" + comboBoxRarity.SelectedItem.ToString() + "'";

            if (float.TryParse(textBoxFromValue.Text, out m))
            {
                textBoxFromValue.BackColor = Color.White;
                queryText += " and a.value >= " + m;
            }
            else
                if (!textBoxFromValue.Text.Equals(""))
                textBoxFromValue.BackColor = Color.Red;
            else
                textBoxFromValue.BackColor = Color.White;

            if (float.TryParse(textBoxToValue.Text, out m))
            {
                textBoxToValue.BackColor = Color.White;
                queryText += " and a.value <= " + m;
            }
            else
                if (!textBoxToValue.Text.Equals(""))
                textBoxToValue.BackColor = Color.Red;
            else
                textBoxToValue.BackColor = Color.White;
        }
        
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Please select a card you wish to add/modify it's quantity first!");
            else
            {
                int m;
                if (int.TryParse(textBoxQuantity.Text, out m))
                    if (m < 1)
                    {
                        MessageBox.Show("Please enter number larger than zero (0) for quantity!");
                        textBoxQuantity.BackColor = Color.Red;
                    }
                    else
                    {
                        textBoxQuantity.BackColor = Color.White;
                        
                        if (mode.Equals("CREATE"))
                        {
                            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + ") WHERE a.id = " + user.id
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " " + mode + " (a)-[r: OWNS{ quantity : " + m + " }]->(b) RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                            ((IRawGraphClient)client).ExecuteCypher(query);

                            ids.RemoveAt(listBox1.SelectedIndex);
                            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                            MessageBox.Show("Succesfully added card to your Collection!");
                        }
                        else
                        {
                            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + user.id
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " SET r.quantity = " + m + " RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                            ((IRawGraphClient)client).ExecuteCypher(query);

                            quantities[listBox1.SelectedIndex] = m;
                            string temp = listBox1.Items[listBox1.SelectedIndex].ToString().Split('\t')[1];
                            listBox1.Items[listBox1.SelectedIndex] = m + "\t" + temp;
                            MessageBox.Show("Succesfully changed the quantity!");
                        }
                        
                    }
                else
                {
                    textBoxQuantity.BackColor = Color.Red;
                    MessageBox.Show("Please enter only numbers for value!");
                }
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSearchMyCollection_Click(object sender, EventArgs e)
        {
            myColl = true;
            labelListBox1.Text = "My Cards";
            searchQuery("", listBox1, ids, ref monsterCards, ref spellCards, ref trapCards, quantities, "=");
            mode = "SET";
            buttonAdd.Text = "Modify Quantity";
            buttonDeleteCard.Visible = true;
        }

        private void buttonDeleteCard_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "),(b:User), b-[r: OWNS]->a where b.id = " + user.id
                                                                + " AND a.id = " + ids[listBox1.SelectedIndex] + " delete r",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query);
                ids.RemoveAt(listBox1.SelectedIndex);
                quantities.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);

                MessageBox.Show("Succesfully removed card from your Collection!");
            }
        }

        private void buttonSearchOtherUsersCollection_Click(object sender, EventArgs e)
        {
            searchQuery("", listBox2, idsOther, ref monsterCardsOther, ref spellCardsOther, ref trapCardsOther, quantitiesOther, "<>");
        }

        private void buttonTrade_Click(object sender, EventArgs e)
        {
            try
            {
                if (!myColl)
                    throw new Exception("Please select from your own cards!");
                if (listBox1.SelectedIndex == -1 || listBox2.SelectedIndex == -1)
                    throw new Exception("Please select cards (your own and other player's) you wish to trade!");
                else
                {
                    int leftQuantity, rightQuantity;

                    leftQuantity = tryParsing(textBoxQuantity);
                    rightQuantity = tryParsing(textBoxQuantityOther);
                    if (quantities[listBox1.SelectedIndex] - leftQuantity < 0)
                        throw new Exception("You do not have enough copies of that card!");
                    if (quantitiesOther[listBox2.SelectedIndex] - rightQuantity < 0)
                        throw new Exception("Other Player does not have enough copies of selected card!");

                    User tradingUser;

                    var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User) WHERE a.username = '" + usernameOther[listBox2.SelectedIndex]
                                                                       + "' RETURN a",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                    tradingUser = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList().FirstOrDefault();

                    //other user gives to us first
                    //this is for us
                    if (ids.Contains(idsOther[listBox2.SelectedIndex]))//just set new quantity
                    {
                        int index = ids.IndexOf(idsOther[listBox2.SelectedIndex]);
                        int setQuantity = rightQuantity + quantities[index];
                        var query1 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + user.id
                                                                       + " AND b.id = " + idsOther[listBox2.SelectedIndex]
                                                                       + " SET r.quantity = " +  setQuantity + " RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteCypher(query1);

                        quantities[index] = setQuantity;

                        string s = listBox1.Items[index].ToString().Split('\t')[1];
                        listBox1.Items[index] = setQuantity + "\t" + s;
                        //arrange listboxes arrays
                    }
                    else
                    {
                        var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + ") WHERE a.id = " + user.id
                                                                       + " AND b.id = " + idsOther[listBox2.SelectedIndex]
                                                                       + " CREATE (a)-[r: OWNS{ quantity : " + rightQuantity + " }]->(b) RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteCypher(query2);

                        quantities.Add(rightQuantity);
                        ids.Add(idsOther[listBox2.SelectedIndex]);
                        string s = listBox2.Items[listBox2.SelectedIndex].ToString().Split('\t')[1];
                        listBox1.Items.Add(rightQuantity + "\t" + s);
                        //arrange listboxes arrays
                    }
                    //this is for other user
                    if (quantitiesOther[listBox2.SelectedIndex] - rightQuantity == 0) //delete relationship
                    {
                        var query3 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "),(b:User), b-[r: OWNS]->a where b.id = " + tradingUser.id
                                                                + " AND a.id = " + idsOther[listBox2.SelectedIndex] + " delete r",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                        ((IRawGraphClient)client).ExecuteCypher(query3);

                        idsOther.RemoveAt(listBox2.SelectedIndex);
                        quantitiesOther.RemoveAt(listBox2.SelectedIndex);
                        usernameOther.RemoveAt(listBox2.SelectedIndex);
                        listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                        //arrange listboxes arrays
                    }
                    else     //set new quantity as difference
                    {
                        int rightDifference = quantitiesOther[listBox2.SelectedIndex] - rightQuantity;
                        var query4 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + tradingUser.id
                                                                       + " AND b.id = " + idsOther[listBox2.SelectedIndex]
                                                                       + " SET r.quantity = " + rightDifference + " RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);
                        ((IRawGraphClient)client).ExecuteCypher(query4);

                        quantitiesOther[listBox2.SelectedIndex] = rightDifference;
                        string s = listBox2.Items[listBox2.SelectedIndex].ToString().Split('\t')[1];
                        listBox2.Items[listBox2.SelectedIndex] = rightDifference + "\t" + s + "\t" + usernameOther[listBox2.SelectedIndex];
                    }


                    //we give to other user
                    var querytemp = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + tradingUser.id
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " RETURN r.quantity",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                    int temp = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(querytemp).ToList().FirstOrDefault();

                    //this is for other user
                    if (temp != 0)//just set new quantity
                    {

                        int index;

                        for (index = 0; index < idsOther.Count; index++)
                            if (idsOther[index] == ids[listBox1.SelectedIndex] && usernameOther[index].Equals(tradingUser.username))
                                break;//usernames!

                        int setQuantity = temp + leftQuantity;
                        var query1 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + tradingUser.id
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " SET r.quantity = " + setQuantity + " RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteCypher(query1);

                        quantitiesOther[index] = setQuantity;
                        string s = listBox2.Items[index].ToString().Split('\t')[1];
                        listBox2.Items[index] = setQuantity + "\t" + s + "\t" + tradingUser.username;
                        
                    }
                    else
                    {
                        var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + ") WHERE a.id = " + tradingUser.id
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " CREATE (a)-[r: OWNS{ quantity : " + leftQuantity + " }]->(b) RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteCypher(query2);

                        quantitiesOther.Add(leftQuantity);
                        usernameOther.Add(tradingUser.username);
                        idsOther.Add(ids[listBox1.SelectedIndex]);
                        string s = listBox1.Items[listBox1.SelectedIndex].ToString().Split('\t')[1];
                        listBox2.Items.Add(rightQuantity + "\t" + s + "\t" + tradingUser.username);
                        //arrange listboxes arrays
                    }
                    //this is for us
                    if (quantities[listBox1.SelectedIndex] - leftQuantity == 0) //delete relationship
                    {
                        var query3 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "),(b:User), b-[r: OWNS]->a where b.id = " + user.id
                                                                + " AND a.id = " + ids[listBox1.SelectedIndex] + " delete r",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                        ((IRawGraphClient)client).ExecuteCypher(query3);

                        ids.RemoveAt(listBox1.SelectedIndex);
                        quantities.RemoveAt(listBox1.SelectedIndex);
                        listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                        //arrange listboxes arrays
                    }
                    else//set new quantity as difference
                    {
                        int leftDifference = quantities[listBox1.SelectedIndex] - leftQuantity;
                        var query4 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + user.id
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " SET r.quantity = " + leftDifference + " RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);
                        ((IRawGraphClient)client).ExecuteCypher(query4);

                        quantities[listBox1.SelectedIndex] = leftDifference;
                        string s = listBox1.Items[listBox1.SelectedIndex].ToString().Split('\t')[1];
                        listBox1.Items[listBox1.SelectedIndex] = leftDifference + "\t" + s;
                    }
                    MessageBox.Show("Successfully traded card with other user!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private int tryParsing(TextBox tb)
        {
            int m;
            if (int.TryParse(tb.Text, out m))
                if (m < 1)
                {
                    tb.BackColor = Color.Red;
                    throw new Exception("Please enter number larger than zero (0) for quantity!");
                }
                else
                {
                    tb.BackColor = Color.White;
                    return m;
                }
            else
            {
                tb.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for quantity!");
            }
        }

        private void buttonSearchShop_Click(object sender, EventArgs e)
        {
            searchQuery("", listBox3, idsShop, ref monsterCardsShop, ref spellCardsShop, ref trapCardsShop, quantitiesShop, "Shop");
        }

        private void buttonTradeShop_Click(object sender, EventArgs e)
        {
            if (!myColl)
                buttonSearchMyCollection_Click( sender, e);
            if (listBox3.SelectedIndex == -1)
                MessageBox.Show("Please select a card you wish to buy first!");
            else
            {
                int m;
                if (int.TryParse(textBoxQuantityShop.Text, out m))
                    if (m < 1 || m > quantitiesShop[listBox3.SelectedIndex])
                    {
                        MessageBox.Show("Number for quantity must be higher than zero (0) and not higher than current quantity available!");
                        textBoxQuantityShop.BackColor = Color.Red;
                    }
                    else
                    {
                        textBoxQuantityShop.BackColor = Color.White;

                        float disc;
                        if (user.club != null)
                            disc = user.club.discount;
                        else
                            disc = 0;
                        string name;
                        float price;

                        if (comboBoxType.SelectedItem.Equals("MonsterCard"))
                        {
                            price = monsterCardsShop[listBox3.SelectedIndex].value - monsterCardsShop[listBox3.SelectedIndex].value * disc;
                            name = monsterCardsShop[listBox3.SelectedIndex].name;
                        }
                        else
                            if (comboBoxType.SelectedItem.Equals("SpellCard"))
                            {
                                price = spellCardsShop[listBox3.SelectedIndex].value - spellCardsShop[listBox3.SelectedIndex].value * disc;
                                name = spellCardsShop[listBox3.SelectedIndex].name;
                            }
                            else
                            {
                                price = trapCardsShop[listBox3.SelectedIndex].value - trapCardsShop[listBox3.SelectedIndex].value * disc;
                                name = trapCardsShop[listBox3.SelectedIndex].name;
                            }
                            
                        DialogResult dialogResult = MessageBox.Show("Do you want to buy " + m + " " + name + " card(s) from shop, for " + price + "?",
                                                    "Confirm Purchase", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            if (user.funds >= price)//new card
                            {
                                int cardID = idsShop[listBox3.SelectedIndex];
                                //shop gives to the user first
                                //this is for the user
                                if (ids.Contains(idsShop[listBox3.SelectedIndex]))//just set new quantity
                                {
                                    int index = ids.IndexOf(cardID);
                                    int setQuantity = m + quantities[index];
                                    var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + "), a-[r:OWNS]->b WHERE a.id = " + user.id
                                                                                    + " AND b.id = " + cardID
                                                                                    + " SET r.quantity = " + setQuantity + " RETURN r",
                                                                                    new Dictionary<string, object>(),
                                                                                    CypherResultMode.Set);
                                    ((IRawGraphClient)client).ExecuteCypher(query);

                                    quantities[index] = setQuantity;

                                    string s = listBox1.Items[index].ToString().Split('\t')[1];
                                    listBox1.Items[index] = setQuantity + "\t" + s;                                        
                                }
                                else
                                {
                                    var query1 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + ") WHERE a.id = " + user.id
                                                                                    + " AND b.id = " + cardID
                                                                                    + " CREATE (a)-[r: OWNS{ quantity : " + m + " }]->(b) RETURN r",
                                                                                    new Dictionary<string, object>(),
                                                                                    CypherResultMode.Set);
                                    ((IRawGraphClient)client).ExecuteCypher(query1);

                                    quantities.Add(m);
                                    ids.Add(cardID);
                                    string s = listBox3.Items[listBox3.SelectedIndex].ToString().Split('\t')[1];
                                    listBox1.Items.Add(m + "\t" + s);
                                }
                                //this is for the shop
                                if (quantitiesShop[listBox3.SelectedIndex] - m == 0) //delete relationship
                                {
                                    var query2 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:Shop), b-[r: SELLS]->a where b.id = 1"
                                                                            + " AND a.id = " + cardID + " delete r",
                                                                            new Dictionary<string, object>(), CypherResultMode.Set);
                                    ((IRawGraphClient)client).ExecuteCypher(query2);

                                    idsShop.RemoveAt(listBox3.SelectedIndex);
                                    quantitiesShop.RemoveAt(listBox3.SelectedIndex);
                                    listBox3.Items.RemoveAt(listBox3.SelectedIndex);
                                }
                                else //set new quantity as difference
                                {
                                    int difference = quantitiesShop[listBox3.SelectedIndex] - m;
                                    var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Shop), (b:" + cardType + "), a-[r:SELLS]->b WHERE a.id = 1"
                                                                                    + " AND b.id = " + cardID
                                                                                    + " SET r.quantity = " + difference + " RETURN r",
                                                                                    new Dictionary<string, object>(),
                                                                                    CypherResultMode.Set);
                                    ((IRawGraphClient)client).ExecuteCypher(query3);

                                    quantitiesShop[listBox3.SelectedIndex] = difference;
                                    string s = listBox3.Items[listBox3.SelectedIndex].ToString().Split('\t')[1];
                                    listBox3.Items[listBox3.SelectedIndex] = difference + "\t" + s;
                                }

                                //exchange money
                                //deduct money from user
                                float newFunds = user.funds - price;
                                var query4 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id = " + user.id + " set n.funds = " + newFunds
                                                                                + " return n", new Dictionary<string, object>(), CypherResultMode.Set);
                                ((IRawGraphClient)client).ExecuteCypher(query4);
                                user.funds = newFunds;

                                //add money to shop
                                var query5 = new Neo4jClient.Cypher.CypherQuery("match (n:Shop) where n.id = 1 return n.balance",
                                    new Dictionary<string, object>(), CypherResultMode.Set);
                                float balance = ((IRawGraphClient)client).ExecuteGetCypherResults<float>(query5).ToList().FirstOrDefault();
                                balance += price;
                                var query6 = new Neo4jClient.Cypher.CypherQuery("match (n:Shop) where n.id = 1 set n.balance = " + balance
                                                                                + " return n", new Dictionary<string, object>(), CypherResultMode.Set);
                                ((IRawGraphClient)client).ExecuteCypher(query6);

                                //raise club discount
                                if (user.club != null)
                                {
                                    float discount = user.club.discount + m * 0.001f;
                                    if (discount > 10)
                                        discount = 10;
                                    var query7 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id = " + user.club.id
                                                                                    + " set n.discount = " + discount
                                                                                    + " return n", new Dictionary<string, object>(), CypherResultMode.Set);
                                    ((IRawGraphClient)client).ExecuteCypher(query7);
                                    user.club.discount = discount;
                                }
                                MessageBox.Show("Successfully bought a card!");
                            }
                            else
                                MessageBox.Show("Insufficient funds!");
                        }
                    }
                else
                {
                    textBoxQuantity.BackColor = Color.Red;
                    MessageBox.Show("Please enter only numbers for value!");
                }
            }
        }
    }
}
