﻿namespace TCG.Forms
{
    partial class AddCardsToShopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelListBox1 = new System.Windows.Forms.Label();
            this.buttonDeleteCard = new System.Windows.Forms.Button();
            this.buttonSearchShopCollection = new System.Windows.Forms.Button();
            this.buttonDone = new System.Windows.Forms.Button();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonSearchDatabase = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxToValue = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFromValue = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxRarity = new System.Windows.Forms.ComboBox();
            this.comboBoxCondition = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.groupBoxTrapCard = new System.Windows.Forms.GroupBox();
            this.comboBoxTrapIcon = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxSpellCard = new System.Windows.Forms.GroupBox();
            this.comboBoxSpellIcon = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBoxMonsterCard = new System.Windows.Forms.GroupBox();
            this.textBoxToDefense = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxFromDefense = new System.Windows.Forms.TextBox();
            this.textBoxToAttack = new System.Windows.Forms.TextBox();
            this.comboBoxMonsterLevel = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxMonsterAttribute = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxMonsterType = new System.Windows.Forms.ComboBox();
            this.textBoxFromAttack = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBoxTrapCard.SuspendLayout();
            this.groupBoxSpellCard.SuspendLayout();
            this.groupBoxMonsterCard.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelListBox1
            // 
            this.labelListBox1.AutoSize = true;
            this.labelListBox1.Location = new System.Drawing.Point(369, 40);
            this.labelListBox1.Name = "labelListBox1";
            this.labelListBox1.Size = new System.Drawing.Size(0, 13);
            this.labelListBox1.TabIndex = 51;
            // 
            // buttonDeleteCard
            // 
            this.buttonDeleteCard.Location = new System.Drawing.Point(372, 429);
            this.buttonDeleteCard.Name = "buttonDeleteCard";
            this.buttonDeleteCard.Size = new System.Drawing.Size(92, 23);
            this.buttonDeleteCard.TabIndex = 49;
            this.buttonDeleteCard.Text = "Delete Card";
            this.buttonDeleteCard.UseVisualStyleBackColor = true;
            this.buttonDeleteCard.Click += new System.EventHandler(this.buttonDeleteCard_Click);
            // 
            // buttonSearchShopCollection
            // 
            this.buttonSearchShopCollection.Location = new System.Drawing.Point(28, 403);
            this.buttonSearchShopCollection.Name = "buttonSearchShopCollection";
            this.buttonSearchShopCollection.Size = new System.Drawing.Size(122, 40);
            this.buttonSearchShopCollection.TabIndex = 48;
            this.buttonSearchShopCollection.Text = "Search Shop Collection";
            this.buttonSearchShopCollection.UseVisualStyleBackColor = true;
            this.buttonSearchShopCollection.Click += new System.EventHandler(this.buttonSearchShopCollection_Click);
            // 
            // buttonDone
            // 
            this.buttonDone.Location = new System.Drawing.Point(484, 467);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(100, 23);
            this.buttonDone.TabIndex = 47;
            this.buttonDone.Text = "Done";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Location = new System.Drawing.Point(484, 403);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantity.TabIndex = 46;
            this.textBoxQuantity.Text = "1";
            this.textBoxQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Location = new System.Drawing.Point(372, 403);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(49, 13);
            this.labelQuantity.TabIndex = 45;
            this.labelQuantity.Text = "Quantity:";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(484, 429);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(100, 23);
            this.buttonAdd.TabIndex = 44;
            this.buttonAdd.Text = "Modify Quantity";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonSearchDatabase
            // 
            this.buttonSearchDatabase.Location = new System.Drawing.Point(203, 403);
            this.buttonSearchDatabase.Name = "buttonSearchDatabase";
            this.buttonSearchDatabase.Size = new System.Drawing.Size(131, 40);
            this.buttonSearchDatabase.TabIndex = 43;
            this.buttonSearchDatabase.Text = "Search Entire Database";
            this.buttonSearchDatabase.UseVisualStyleBackColor = true;
            this.buttonSearchDatabase.Click += new System.EventHandler(this.buttonSearchDatabase_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxToValue);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBoxType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxFromValue);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.comboBoxRarity);
            this.groupBox1.Controls.Add(this.comboBoxCondition);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Location = new System.Drawing.Point(28, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 184);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Information:";
            // 
            // textBoxToValue
            // 
            this.textBoxToValue.Location = new System.Drawing.Point(246, 141);
            this.textBoxToValue.Name = "textBoxToValue";
            this.textBoxToValue.Size = new System.Drawing.Size(54, 20);
            this.textBoxToValue.TabIndex = 17;
            this.textBoxToValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(220, 148);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "To";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(111, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "From:";
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "MonsterCard",
            "SpellCard",
            "TrapCard"});
            this.comboBoxType.Location = new System.Drawing.Point(112, 34);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(188, 21);
            this.comboBoxType.TabIndex = 14;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Type:";
            // 
            // textBoxFromValue
            // 
            this.textBoxFromValue.Location = new System.Drawing.Point(150, 141);
            this.textBoxFromValue.Name = "textBoxFromValue";
            this.textBoxFromValue.Size = new System.Drawing.Size(48, 20);
            this.textBoxFromValue.TabIndex = 12;
            this.textBoxFromValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Value:";
            // 
            // comboBoxRarity
            // 
            this.comboBoxRarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRarity.FormattingEnabled = true;
            this.comboBoxRarity.Items.AddRange(new object[] {
            "Common",
            "Rare",
            "Super Rare",
            "Ultra Rare",
            "Secret Rare",
            "Ultimate Rare",
            "Gold Rare",
            "Ghost Rare"});
            this.comboBoxRarity.Location = new System.Drawing.Point(112, 113);
            this.comboBoxRarity.Name = "comboBoxRarity";
            this.comboBoxRarity.Size = new System.Drawing.Size(188, 21);
            this.comboBoxRarity.TabIndex = 10;
            // 
            // comboBoxCondition
            // 
            this.comboBoxCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCondition.FormattingEnabled = true;
            this.comboBoxCondition.Items.AddRange(new object[] {
            "Near Mint",
            "Used",
            "Damaged"});
            this.comboBoxCondition.Location = new System.Drawing.Point(112, 86);
            this.comboBoxCondition.Name = "comboBoxCondition";
            this.comboBoxCondition.Size = new System.Drawing.Size(188, 21);
            this.comboBoxCondition.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Rarity: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Condition";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(112, 59);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(188, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // groupBoxTrapCard
            // 
            this.groupBoxTrapCard.Controls.Add(this.comboBoxTrapIcon);
            this.groupBoxTrapCard.Controls.Add(this.label6);
            this.groupBoxTrapCard.Location = new System.Drawing.Point(31, 242);
            this.groupBoxTrapCard.Name = "groupBoxTrapCard";
            this.groupBoxTrapCard.Size = new System.Drawing.Size(303, 44);
            this.groupBoxTrapCard.TabIndex = 41;
            this.groupBoxTrapCard.TabStop = false;
            this.groupBoxTrapCard.Text = "Additional Information:";
            this.groupBoxTrapCard.Visible = false;
            // 
            // comboBoxTrapIcon
            // 
            this.comboBoxTrapIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTrapIcon.FormattingEnabled = true;
            this.comboBoxTrapIcon.Items.AddRange(new object[] {
            "Continous",
            "Counter",
            "Normal"});
            this.comboBoxTrapIcon.Location = new System.Drawing.Point(109, 15);
            this.comboBoxTrapIcon.Name = "comboBoxTrapIcon";
            this.comboBoxTrapIcon.Size = new System.Drawing.Size(188, 21);
            this.comboBoxTrapIcon.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Icon:";
            // 
            // groupBoxSpellCard
            // 
            this.groupBoxSpellCard.Controls.Add(this.comboBoxSpellIcon);
            this.groupBoxSpellCard.Controls.Add(this.label5);
            this.groupBoxSpellCard.Location = new System.Drawing.Point(31, 242);
            this.groupBoxSpellCard.Name = "groupBoxSpellCard";
            this.groupBoxSpellCard.Size = new System.Drawing.Size(303, 44);
            this.groupBoxSpellCard.TabIndex = 40;
            this.groupBoxSpellCard.TabStop = false;
            this.groupBoxSpellCard.Text = "Additional Information:";
            this.groupBoxSpellCard.Visible = false;
            // 
            // comboBoxSpellIcon
            // 
            this.comboBoxSpellIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpellIcon.FormattingEnabled = true;
            this.comboBoxSpellIcon.Items.AddRange(new object[] {
            "Continuous",
            "Equip",
            "Field",
            "Normal",
            "Ritual",
            "Quick Play"});
            this.comboBoxSpellIcon.Location = new System.Drawing.Point(109, 16);
            this.comboBoxSpellIcon.Name = "comboBoxSpellIcon";
            this.comboBoxSpellIcon.Size = new System.Drawing.Size(188, 21);
            this.comboBoxSpellIcon.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Icon:";
            // 
            // groupBoxMonsterCard
            // 
            this.groupBoxMonsterCard.Controls.Add(this.textBoxToDefense);
            this.groupBoxMonsterCard.Controls.Add(this.label17);
            this.groupBoxMonsterCard.Controls.Add(this.label18);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxFromDefense);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxToAttack);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterLevel);
            this.groupBoxMonsterCard.Controls.Add(this.label15);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterAttribute);
            this.groupBoxMonsterCard.Controls.Add(this.label16);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterType);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxFromAttack);
            this.groupBoxMonsterCard.Controls.Add(this.label10);
            this.groupBoxMonsterCard.Controls.Add(this.label9);
            this.groupBoxMonsterCard.Controls.Add(this.label8);
            this.groupBoxMonsterCard.Controls.Add(this.label7);
            this.groupBoxMonsterCard.Controls.Add(this.label4);
            this.groupBoxMonsterCard.Location = new System.Drawing.Point(28, 242);
            this.groupBoxMonsterCard.Name = "groupBoxMonsterCard";
            this.groupBoxMonsterCard.Size = new System.Drawing.Size(306, 150);
            this.groupBoxMonsterCard.TabIndex = 39;
            this.groupBoxMonsterCard.TabStop = false;
            this.groupBoxMonsterCard.Text = "Additional Information:";
            this.groupBoxMonsterCard.Visible = false;
            // 
            // textBoxToDefense
            // 
            this.textBoxToDefense.Location = new System.Drawing.Point(246, 121);
            this.textBoxToDefense.Name = "textBoxToDefense";
            this.textBoxToDefense.Size = new System.Drawing.Size(54, 20);
            this.textBoxToDefense.TabIndex = 25;
            this.textBoxToDefense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(220, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "To";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(111, 128);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "From:";
            // 
            // textBoxFromDefense
            // 
            this.textBoxFromDefense.Location = new System.Drawing.Point(150, 121);
            this.textBoxFromDefense.Name = "textBoxFromDefense";
            this.textBoxFromDefense.Size = new System.Drawing.Size(48, 20);
            this.textBoxFromDefense.TabIndex = 22;
            this.textBoxFromDefense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxToAttack
            // 
            this.textBoxToAttack.Location = new System.Drawing.Point(246, 95);
            this.textBoxToAttack.Name = "textBoxToAttack";
            this.textBoxToAttack.Size = new System.Drawing.Size(54, 20);
            this.textBoxToAttack.TabIndex = 21;
            this.textBoxToAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBoxMonsterLevel
            // 
            this.comboBoxMonsterLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterLevel.FormattingEnabled = true;
            this.comboBoxMonsterLevel.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxMonsterLevel.Location = new System.Drawing.Point(112, 71);
            this.comboBoxMonsterLevel.Name = "comboBoxMonsterLevel";
            this.comboBoxMonsterLevel.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterLevel.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(220, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "To";
            // 
            // comboBoxMonsterAttribute
            // 
            this.comboBoxMonsterAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterAttribute.FormattingEnabled = true;
            this.comboBoxMonsterAttribute.Items.AddRange(new object[] {
            "Dark",
            "Divine",
            "Earth",
            "Fire",
            "Light",
            "Water",
            "Wind"});
            this.comboBoxMonsterAttribute.Location = new System.Drawing.Point(112, 44);
            this.comboBoxMonsterAttribute.Name = "comboBoxMonsterAttribute";
            this.comboBoxMonsterAttribute.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterAttribute.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(111, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "From:";
            // 
            // comboBoxMonsterType
            // 
            this.comboBoxMonsterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterType.FormattingEnabled = true;
            this.comboBoxMonsterType.Items.AddRange(new object[] {
            "Aqua",
            "Beast",
            "Beast-Warrior",
            "Creator God",
            "Dinosaur",
            "Divine-Beast",
            "Dragon",
            "Fairy",
            "Fiend",
            "Fish",
            "Insect",
            "Machine",
            "Plant",
            "Psychic",
            "Pyro",
            "Reptile",
            "Rock",
            "Sea Serpent",
            "Spellcaster",
            "Thunder",
            "Warrior",
            "Winged Beast",
            "Zombie"});
            this.comboBoxMonsterType.Location = new System.Drawing.Point(112, 18);
            this.comboBoxMonsterType.Name = "comboBoxMonsterType";
            this.comboBoxMonsterType.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterType.TabIndex = 9;
            // 
            // textBoxFromAttack
            // 
            this.textBoxFromAttack.Location = new System.Drawing.Point(150, 95);
            this.textBoxFromAttack.Name = "textBoxFromAttack";
            this.textBoxFromAttack.Size = new System.Drawing.Size(48, 20);
            this.textBoxFromAttack.TabIndex = 18;
            this.textBoxFromAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Defense:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Attack:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Level:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Attribute:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Type:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(372, 75);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(212, 316);
            this.listBox1.TabIndex = 38;
            // 
            // AddCardsToShopForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 571);
            this.Controls.Add(this.labelListBox1);
            this.Controls.Add(this.buttonDeleteCard);
            this.Controls.Add(this.buttonSearchShopCollection);
            this.Controls.Add(this.buttonDone);
            this.Controls.Add(this.textBoxQuantity);
            this.Controls.Add(this.labelQuantity);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonSearchDatabase);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxTrapCard);
            this.Controls.Add(this.groupBoxSpellCard);
            this.Controls.Add(this.groupBoxMonsterCard);
            this.Controls.Add(this.listBox1);
            this.Name = "AddCardsToShopForm";
            this.Text = "AddCardsToShopForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxTrapCard.ResumeLayout(false);
            this.groupBoxTrapCard.PerformLayout();
            this.groupBoxSpellCard.ResumeLayout(false);
            this.groupBoxSpellCard.PerformLayout();
            this.groupBoxMonsterCard.ResumeLayout(false);
            this.groupBoxMonsterCard.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelListBox1;
        private System.Windows.Forms.Button buttonDeleteCard;
        private System.Windows.Forms.Button buttonSearchShopCollection;
        private System.Windows.Forms.Button buttonDone;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonSearchDatabase;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxToValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFromValue;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxRarity;
        private System.Windows.Forms.ComboBox comboBoxCondition;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.GroupBox groupBoxTrapCard;
        private System.Windows.Forms.ComboBox comboBoxTrapIcon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxSpellCard;
        private System.Windows.Forms.ComboBox comboBoxSpellIcon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBoxMonsterCard;
        private System.Windows.Forms.TextBox textBoxToDefense;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxFromDefense;
        private System.Windows.Forms.TextBox textBoxToAttack;
        private System.Windows.Forms.ComboBox comboBoxMonsterLevel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBoxMonsterAttribute;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBoxMonsterType;
        private System.Windows.Forms.TextBox textBoxFromAttack;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox1;
    }
}