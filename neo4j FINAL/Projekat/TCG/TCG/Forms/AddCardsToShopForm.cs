﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class AddCardsToShopForm : Form
    {
        private GraphClient client;
        private string cardType;
        private string queryText;
        private string mode;
        private List<MonsterCard> monsterCards;
        private List<SpellCard> spellCards;
        private List<TrapCard> trapCards;
        private List<int> ids;
        private List<int> quantities;

        public AddCardsToShopForm(GraphClient cl)
        {
            InitializeComponent();
            client = cl;
            cardType = "";
            queryText = "";
            ids = new List<int>();
            quantities = new List<int>();
            monsterCards = new List<MonsterCard>();
            spellCards = new List<SpellCard>();
            trapCards = new List<TrapCard>();
        }

        private void buttonSearchShopCollection_Click(object sender, EventArgs e)
        {
            labelListBox1.Text = "Shop Cards";
            searchQuery("", listBox1, ids, monsterCards, spellCards, trapCards, quantities);
            mode = "SET";
            buttonAdd.Text = "Modify Quantity";
            buttonDeleteCard.Visible = true;
        }

        private void buttonSearchDatabase_Click(object sender, EventArgs e)
        {
            labelListBox1.Text = "All Cards";
            searchQuery("not ", listBox1, ids, monsterCards, spellCards, trapCards, quantities);
            mode = "CREATE";
            buttonAdd.Text = "Add Card";
            buttonDeleteCard.Visible = false;
        }

        private void searchQuery(string mode, ListBox lB, List<int> IDs, List<MonsterCard> mc, List<SpellCard> sc, List<TrapCard> tc, List<int> quant)
        {
            quant.Clear();
            lB.Items.Clear();
            IDs.Clear();
            mc.Clear();
            sc.Clear();
            tc.Clear();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            if (comboBoxType.SelectedIndex == 0)
            {
                commonQueryProperties();
                if (comboBoxMonsterType.SelectedIndex != -1)
                    queryText += " and a.type = '" + comboBoxMonsterType.SelectedItem.ToString() + "'";
                if (comboBoxMonsterAttribute.SelectedIndex != -1)
                    queryText += " and a.attribute = '" + comboBoxMonsterAttribute.SelectedItem.ToString() + "'";
                if (comboBoxMonsterLevel.SelectedIndex != -1)
                    queryText += " and a.level = " + comboBoxMonsterLevel.SelectedItem.ToString();
                int m;
                if (int.TryParse(textBoxFromAttack.Text, out m))
                    queryText += " and a.attack >= " + m;

                if (int.TryParse(textBoxToAttack.Text, out m))
                    queryText += " and a.attack <= " + m;

                if (int.TryParse(textBoxFromDefense.Text, out m))
                    queryText += " and a.defense >= " + m;

                if (int.TryParse(textBoxToDefense.Text, out m))
                    queryText += " and a.defense <= " + m;
                

                queryText += " and " + mode + "b-[:SELLS]->a return a";

                var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                mc = ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();

                foreach (MonsterCard mC in mc)
                    fillQuantities(mC.id, mC.name, mode, lB, IDs, quant);
            }
            else
                if (comboBoxType.SelectedIndex == 1)
                {
                    commonQueryProperties();
                    if (comboBoxSpellIcon.SelectedIndex != -1)
                        queryText += " and a.icon = '" + comboBoxSpellIcon.SelectedItem.ToString() + "'";

                    queryText += " and " + mode + "b-[:SELLS]->a return a";

                    var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                    sc = ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();

                    foreach (SpellCard sC in sc)
                        fillQuantities(sC.id, sC.name, mode, lB, IDs, quant);
                }
                else
                    if (comboBoxType.SelectedIndex == 2)
                    {
                        commonQueryProperties();
                        if (comboBoxTrapIcon.SelectedIndex != -1)
                            queryText += " and a.icon = '" + comboBoxTrapIcon.SelectedItem.ToString() + "'";

                        queryText += " and " + mode + "b-[:SELLS]->a return a";

                        var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                        tc = ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();

                        foreach (TrapCard tC in tc)
                            fillQuantities(tC.id, tC.name, mode, lB, IDs, quant);
                    }
                    else
                        MessageBox.Show("Please select type of card first!");
        }

        private void commonQueryProperties()
        {
            float m;
            queryText = "match (a:" + cardType + "), (b:Shop) where b.id = 1";

            if (textBoxName.Text != "")
                queryText += " and a.name = '" + textBoxName.Text + "'";
            if (comboBoxCondition.SelectedIndex != -1)
                queryText += " and a.condition = '" + comboBoxCondition.SelectedItem.ToString() + "'";
            if (comboBoxRarity.SelectedIndex != -1)
                queryText += " and a.rarity = '" + comboBoxRarity.SelectedItem.ToString() + "'";

            if (float.TryParse(textBoxFromValue.Text, out m))
                queryText += " and a.value >= " + m;

            if (float.TryParse(textBoxToValue.Text, out m))
                queryText += " and a.value <= " + m;

        }

        private void fillQuantities(int id, string name, string mode, ListBox lB, List<int> IDs, List<int> quant)
        {
            IDs.Add(Convert.ToInt32(id));
            if (mode.Equals(""))
            {
                var query2 = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:Shop), b-[r:SELLS]->a where b.id = 1"
                                                        + " and a.id = " + id + " return r.quantity",
                                                        new Dictionary<string, object>(), CypherResultMode.Set);
                quant.Add(((IRawGraphClient)client).ExecuteGetCypherResults<int>(query2).ToList().FirstOrDefault());
                lB.Items.Add(quant.Last() + "\t" + name);
            }
            else
                lB.Items.Add(name);
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (comboBoxType.SelectedItem.Equals("MonsterCard"))
            {
                cardType = "MonsterCard";
                groupBoxMonsterCard.Visible = true;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("SpellCard"))
            {
                cardType = "SpellCard";
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = true;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("TrapCard"))
            {
                cardType = "TrapCard";
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = true;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Please select a card you wish to add/modify it's quantity first!");
            else
            {
                int m;
                if (int.TryParse(textBoxQuantity.Text, out m))
                    if (m < 1)
                    {
                        MessageBox.Show("Please enter number larger than zero (0) for quantity!");
                        textBoxQuantity.BackColor = Color.Red;
                    }
                    else
                    {
                        textBoxQuantity.BackColor = Color.White;

                        if (mode.Equals("CREATE"))
                        {
                            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Shop), (b:" + cardType + ") WHERE a.id = 1"
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " " + mode + " (a)-[r: SELLS{ quantity : " + m + " }]->(b) RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                            ((IRawGraphClient)client).ExecuteCypher(query);

                            ids.RemoveAt(listBox1.SelectedIndex);
                            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                            MessageBox.Show("Succesfully added card to the Shop!");
                        }
                        else
                        {
                            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Shop), (b:" + cardType + "), a-[r:SELLS]->b WHERE a.id = 1"
                                                                       + " AND b.id = " + ids[listBox1.SelectedIndex]
                                                                       + " SET r.quantity = " + m + " RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                            ((IRawGraphClient)client).ExecuteCypher(query);

                            quantities[listBox1.SelectedIndex] = m;
                            string temp = listBox1.Items[listBox1.SelectedIndex].ToString().Split('\t')[1];
                            listBox1.Items[listBox1.SelectedIndex] = m + "\t" + temp;
                            MessageBox.Show("Succesfully changed the quantity!");
                        }

                    }
                else
                {
                    textBoxQuantity.BackColor = Color.Red;
                    MessageBox.Show("Please enter only numbers for value!");
                }
            }
        }

        private void buttonDeleteCard_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("match (a:" + cardType + "), (b:Shop), b-[r: SELLS]->a where b.id = 1"
                                                                + " AND a.id = " + ids[listBox1.SelectedIndex] + " delete r",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query);
                ids.RemoveAt(listBox1.SelectedIndex);
                quantities.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);

                MessageBox.Show("Succesfully removed card from sales!");
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
