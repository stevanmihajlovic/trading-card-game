﻿namespace TCG.Forms
{
    partial class AdminAccessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAddMonsterCard = new System.Windows.Forms.Button();
            this.buttonModifyMonsterCard = new System.Windows.Forms.Button();
            this.buttonAddSpellCard = new System.Windows.Forms.Button();
            this.buttonAddTrapCard = new System.Windows.Forms.Button();
            this.buttonModifySpellCard = new System.Windows.Forms.Button();
            this.buttonModifyTrapCard = new System.Windows.Forms.Button();
            this.buttonModifyShop = new System.Windows.Forms.Button();
            this.buttonChangeMonth = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Monster Cards:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(184, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Spell Cards:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(344, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Trap Cards:";
            // 
            // buttonAddMonsterCard
            // 
            this.buttonAddMonsterCard.Location = new System.Drawing.Point(16, 45);
            this.buttonAddMonsterCard.Name = "buttonAddMonsterCard";
            this.buttonAddMonsterCard.Size = new System.Drawing.Size(95, 23);
            this.buttonAddMonsterCard.TabIndex = 3;
            this.buttonAddMonsterCard.Text = "Add";
            this.buttonAddMonsterCard.UseVisualStyleBackColor = true;
            this.buttonAddMonsterCard.Click += new System.EventHandler(this.buttonAddMonsterCard_Click);
            // 
            // buttonModifyMonsterCard
            // 
            this.buttonModifyMonsterCard.Location = new System.Drawing.Point(16, 88);
            this.buttonModifyMonsterCard.Name = "buttonModifyMonsterCard";
            this.buttonModifyMonsterCard.Size = new System.Drawing.Size(95, 23);
            this.buttonModifyMonsterCard.TabIndex = 4;
            this.buttonModifyMonsterCard.Text = "Modify or Delete";
            this.buttonModifyMonsterCard.UseVisualStyleBackColor = true;
            this.buttonModifyMonsterCard.Click += new System.EventHandler(this.buttonModifyMonsterCard_Click);
            // 
            // buttonAddSpellCard
            // 
            this.buttonAddSpellCard.Location = new System.Drawing.Point(168, 45);
            this.buttonAddSpellCard.Name = "buttonAddSpellCard";
            this.buttonAddSpellCard.Size = new System.Drawing.Size(95, 23);
            this.buttonAddSpellCard.TabIndex = 6;
            this.buttonAddSpellCard.Text = "Add";
            this.buttonAddSpellCard.UseVisualStyleBackColor = true;
            this.buttonAddSpellCard.Click += new System.EventHandler(this.buttonAddSpellCard_Click);
            // 
            // buttonAddTrapCard
            // 
            this.buttonAddTrapCard.Location = new System.Drawing.Point(327, 45);
            this.buttonAddTrapCard.Name = "buttonAddTrapCard";
            this.buttonAddTrapCard.Size = new System.Drawing.Size(95, 23);
            this.buttonAddTrapCard.TabIndex = 7;
            this.buttonAddTrapCard.Text = "Add";
            this.buttonAddTrapCard.UseVisualStyleBackColor = true;
            this.buttonAddTrapCard.Click += new System.EventHandler(this.buttonAddTrapCard_Click);
            // 
            // buttonModifySpellCard
            // 
            this.buttonModifySpellCard.Location = new System.Drawing.Point(168, 88);
            this.buttonModifySpellCard.Name = "buttonModifySpellCard";
            this.buttonModifySpellCard.Size = new System.Drawing.Size(95, 23);
            this.buttonModifySpellCard.TabIndex = 8;
            this.buttonModifySpellCard.Text = "Modify or Delete";
            this.buttonModifySpellCard.UseVisualStyleBackColor = true;
            this.buttonModifySpellCard.Click += new System.EventHandler(this.buttonModifySpellCard_Click);
            // 
            // buttonModifyTrapCard
            // 
            this.buttonModifyTrapCard.Location = new System.Drawing.Point(327, 88);
            this.buttonModifyTrapCard.Name = "buttonModifyTrapCard";
            this.buttonModifyTrapCard.Size = new System.Drawing.Size(95, 23);
            this.buttonModifyTrapCard.TabIndex = 9;
            this.buttonModifyTrapCard.Text = "Modify or Delete";
            this.buttonModifyTrapCard.UseVisualStyleBackColor = true;
            this.buttonModifyTrapCard.Click += new System.EventHandler(this.buttonModifyTrapCard_Click);
            // 
            // buttonModifyShop
            // 
            this.buttonModifyShop.Location = new System.Drawing.Point(95, 179);
            this.buttonModifyShop.Name = "buttonModifyShop";
            this.buttonModifyShop.Size = new System.Drawing.Size(95, 40);
            this.buttonModifyShop.TabIndex = 11;
            this.buttonModifyShop.Text = "Add Cards To Shop";
            this.buttonModifyShop.UseVisualStyleBackColor = true;
            this.buttonModifyShop.Click += new System.EventHandler(this.buttonModifyShop_Click);
            // 
            // buttonChangeMonth
            // 
            this.buttonChangeMonth.Location = new System.Drawing.Point(246, 179);
            this.buttonChangeMonth.Name = "buttonChangeMonth";
            this.buttonChangeMonth.Size = new System.Drawing.Size(95, 40);
            this.buttonChangeMonth.TabIndex = 12;
            this.buttonChangeMonth.Text = "Monthly Fee for New Month";
            this.buttonChangeMonth.UseVisualStyleBackColor = true;
            this.buttonChangeMonth.Click += new System.EventHandler(this.buttonChangeMonth_Click);
            // 
            // AdminAccessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 231);
            this.Controls.Add(this.buttonChangeMonth);
            this.Controls.Add(this.buttonModifyShop);
            this.Controls.Add(this.buttonModifyTrapCard);
            this.Controls.Add(this.buttonModifySpellCard);
            this.Controls.Add(this.buttonAddTrapCard);
            this.Controls.Add(this.buttonAddSpellCard);
            this.Controls.Add(this.buttonModifyMonsterCard);
            this.Controls.Add(this.buttonAddMonsterCard);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AdminAccessForm";
            this.Text = "AdminAccessForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAddMonsterCard;
        private System.Windows.Forms.Button buttonModifyMonsterCard;
        private System.Windows.Forms.Button buttonAddSpellCard;
        private System.Windows.Forms.Button buttonAddTrapCard;
        private System.Windows.Forms.Button buttonModifySpellCard;
        private System.Windows.Forms.Button buttonModifyTrapCard;
        private System.Windows.Forms.Button buttonModifyShop;
        private System.Windows.Forms.Button buttonChangeMonth;
    }
}