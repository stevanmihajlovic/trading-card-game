﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class AdminAccessForm : Form
    {
        public GraphClient client;
        public AdminAccessForm()
        {
            InitializeComponent();
        }

        private void buttonAddMonsterCard_Click(object sender, EventArgs e)
        {
            addCard(1);
        }

        private void buttonAddSpellCard_Click(object sender, EventArgs e)
        {
            addCard(2);
        }

        private void buttonAddTrapCard_Click(object sender, EventArgs e)
        {
            addCard(3);
        }
        private void addCard(int a)
        {
            AddCardForm reg = new AddCardForm(a);
            reg.client = this.client;
            reg.ShowDialog();
        }

        private void buttonModifyMonsterCard_Click(object sender, EventArgs e)
        {
            modifyCard("MonsterCard");
        }

        private void buttonModifySpellCard_Click(object sender, EventArgs e)
        {
            modifyCard("SpellCard");
        }

        private void buttonModifyTrapCard_Click(object sender, EventArgs e)
        {
            modifyCard("TrapCard");
        }
        private void modifyCard(string a)
        {
            ModifyCardForm reg = new ModifyCardForm(a, client);
            reg.ShowDialog();
        }

        private void buttonModifyShop_Click(object sender, EventArgs e)
        {
            AddCardsToShopForm shop = new AddCardsToShopForm(client);
            shop.ShowDialog();
        }

        private void buttonChangeMonth_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Club) return n;",
                                              new Dictionary<string, object>(), CypherResultMode.Set);
            List<Club> clubs = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList();

            foreach (Club cl in clubs)
            {
                var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:User), (c:Club), n-[MEMBER]->c where c.id = " + cl.id
                                                                            + " return n", new Dictionary<string, object>(), CypherResultMode.Set);

                cl.members = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query1).ToList();

                float deposit = cl.funds;

                foreach (User u in cl.members)
                {
                    if (!u.isLeader)//members
                    {
                        if (u.funds < cl.monthlyFee)//kick user
                        {
                            var query2 = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User), b-[r: MEMBER]->a where b.id = "
                                                                + u.id
                                                                + " AND a.id = " + cl.id + " delete r",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                            ((IRawGraphClient)client).ExecuteCypher(query2);
                        }
                        else//pay monthly fee
                        {
                            deposit += cl.monthlyFee;

                            float m = u.funds - cl.monthlyFee;
                            var query3 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id = " + u.id + " set n.funds = " + m
                                                                    + " return n", new Dictionary<string, object>(), CypherResultMode.Set);
                            ((IRawGraphClient)client).ExecuteCypher(query3);
                        }
                    }
                }
                //increase club funds
                var query4 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id = " + cl.id + " set n.funds = " + deposit
                                                                   + " return n", new Dictionary<string, object>(), CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query4);
                //reset discount
                var query5 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id = " + cl.id + " set n.discount = 0"
                                                                   + " return n", new Dictionary<string, object>(), CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query5);
            }
            MessageBox.Show("Players in all clubs are charged for their club monthly fee or kicked out if they did not have enough money, discount of all clubs is reseted to 0");
        }
    }
}
