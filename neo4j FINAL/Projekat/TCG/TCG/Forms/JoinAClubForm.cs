﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class JoinAClubForm : Form
    {
        public GraphClient client;
        public List<Club> clubs;
        public User user;

        public JoinAClubForm(GraphClient cl, List<Club> clu, User us)
        {
            client = cl;
            InitializeComponent();
            clubs = clu;
            user = us;

            string[] arr = new string[4];
            ListViewItem itm;
            foreach (Club club in clubs)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("match (n:User), (c:Club), n-[MEMBER]->c where c.id = " + club.id
                                                                           + " return n", new Dictionary<string, object>(), CypherResultMode.Set);

                club.members = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();

                arr[0] = club.name;
                arr[1] = club.members.Count.ToString();
                arr[2] = club.discount.ToString()+"%";
                arr[3] = club.monthlyFee.ToString()+" EUR";
                itm = new ListViewItem(arr);
                listView1.Items.Add(itm);
            }
        }

        private void buttonJoinClub_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                MessageBox.Show("Please select a club you wish to join first!");
            else
            {
                int index = listView1.SelectedIndices[0];
                if (user.funds >= clubs[index].monthlyFee)
                {
                    //create relationship
                    var query = new Neo4jClient.Cypher.CypherQuery("MATCH(a: User),(b:Club) WHERE a.username =~ '" + user.username
                                                                    + "' AND b.name =~ '" + clubs[index].name + "' " +
                                                                    "CREATE(a) -[r: MEMBER{ isLeader : 'false' }]->(b) RETURN r",
                                                                    new Dictionary<string, object>(),
                                                                    CypherResultMode.Set);

                    ((IRawGraphClient)client).ExecuteCypher(query);

                    user.club = clubs[index];
                    
                    //deduct money from user
                    float m = user.funds - clubs[index].monthlyFee;
                    var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id = " + user.id + " set n.funds = " + m
                                                                    + " return n", new Dictionary<string, object>(), CypherResultMode.Set);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query1).ToList();
                    user.funds = m;

                    //add money to club
                    float temp = clubs[index].funds + clubs[index].monthlyFee;
                    var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id = " + clubs[index].id + " set n.funds = " + temp
                                                                   + " return n", new Dictionary<string, object>(), CypherResultMode.Set);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query2).ToList();

                    clubs[index].funds = temp;

                    MessageBox.Show("You have successfully joined the club " + clubs[index].name + "!");

                    Close();
                }
                else
                    MessageBox.Show("Insufficient funds, you need to deposit one monthly fee when joining the club!");
            }
        }

        private void buttonCancel_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
