﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    
    public partial class LogInForm : Form
    {
        public GraphClient client;

        public LogInForm()
        {
            InitializeComponent();
        }

        private void logInButton_Click(object sender, EventArgs e)
        {
            String username = usernameTextBox.Text;
            String password = passwordTextBox.Text;

            //find user that matches the inputs
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User) WHERE n.username =~'"+username+"' and n.password =~'"+password+"' return n;",
                                               new Dictionary<string, object>(), CypherResultMode.Set);

            List<User> users = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();

            if (users.Count == 0)
                MessageBox.Show("Wrong username or password!");
            else if (users.Count == 1)
            {
                UserForm uf = new UserForm(users[0], client);
                this.Dispose();
                uf.ShowDialog();              
            }
        }    
    }
}
