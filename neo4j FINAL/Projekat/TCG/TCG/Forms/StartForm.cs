﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class StartForm : Form
    {
        public GraphClient client;
        public StartForm()
        {
            InitializeComponent();
        }        
       
        private void registerButton_Click(object sender, EventArgs e)
        {
            RegisterForm reg = new RegisterForm();
            reg.client = this.client;
            reg.ShowDialog();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "12345");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            //create Shop if it doesnt exist when loading 
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Shop) return n;",
                                              new Dictionary<string, object>(), CypherResultMode.Set);
            List<Shop> shops = ((IRawGraphClient)client).ExecuteGetCypherResults<Shop>(query).ToList();
            if (shops.Count == 0)
            {
                Shop shop = new Shop();

                shop.id = 1;
                shop.name = "Shop";

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("name", shop.name);
                queryDict.Add("balance", shop.balance);
                queryDict.Add("__type__", "org.neo4j.TCG.domain.Shop");

                var query1 = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Shop {id: " + shop.id + ", name:'" + shop.name
                                                                + "', balance: " + shop.balance
                                                                + ", __type__:'org.neo4j.TCG.domain.Shop'}) return n",
                                                                queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query1);
                MessageBox.Show("Shop created! Modify cards that are offered in the Shop next!");
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            LogInForm form = new LogInForm();
            form.client = client;
            form.ShowDialog();
        }

        private void buttonAdminAccess_Click(object sender, EventArgs e)
        {
            AdminAccessForm reg = new AdminAccessForm();
            reg.client = this.client;
            reg.ShowDialog();
        }
    }
}
