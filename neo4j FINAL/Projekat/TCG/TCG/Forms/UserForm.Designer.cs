﻿namespace TCG.Forms
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fundsLabel = new System.Windows.Forms.Label();
            this.clubLabel = new System.Windows.Forms.Label();
            this.lastnameLabel = new System.Windows.Forms.Label();
            this.firstnameLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.buttonJoinCLub = new System.Windows.Forms.Button();
            this.buttonLeaveClub = new System.Windows.Forms.Button();
            this.buttonStartClub = new System.Windows.Forms.Button();
            this.buttonModifyClub = new System.Windows.Forms.Button();
            this.buttonAddCardToMyCollection = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxDeposit = new System.Windows.Forms.TextBox();
            this.buttonDeposit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "First name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Last name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Club:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Funds:";
            // 
            // fundsLabel
            // 
            this.fundsLabel.AutoSize = true;
            this.fundsLabel.Location = new System.Drawing.Point(90, 126);
            this.fundsLabel.Name = "fundsLabel";
            this.fundsLabel.Size = new System.Drawing.Size(0, 13);
            this.fundsLabel.TabIndex = 9;
            // 
            // clubLabel
            // 
            this.clubLabel.AutoSize = true;
            this.clubLabel.Location = new System.Drawing.Point(90, 102);
            this.clubLabel.Name = "clubLabel";
            this.clubLabel.Size = new System.Drawing.Size(76, 13);
            this.clubLabel.TabIndex = 8;
            this.clubLabel.Text = "Not a member.";
            // 
            // lastnameLabel
            // 
            this.lastnameLabel.AutoSize = true;
            this.lastnameLabel.Location = new System.Drawing.Point(90, 79);
            this.lastnameLabel.Name = "lastnameLabel";
            this.lastnameLabel.Size = new System.Drawing.Size(0, 13);
            this.lastnameLabel.TabIndex = 7;
            // 
            // firstnameLabel
            // 
            this.firstnameLabel.AutoSize = true;
            this.firstnameLabel.Location = new System.Drawing.Point(90, 57);
            this.firstnameLabel.Name = "firstnameLabel";
            this.firstnameLabel.Size = new System.Drawing.Size(0, 13);
            this.firstnameLabel.TabIndex = 6;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(90, 34);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(0, 13);
            this.usernameLabel.TabIndex = 5;
            // 
            // buttonJoinCLub
            // 
            this.buttonJoinCLub.Location = new System.Drawing.Point(171, 97);
            this.buttonJoinCLub.Name = "buttonJoinCLub";
            this.buttonJoinCLub.Size = new System.Drawing.Size(75, 23);
            this.buttonJoinCLub.TabIndex = 10;
            this.buttonJoinCLub.Text = "Join a Club";
            this.buttonJoinCLub.UseVisualStyleBackColor = true;
            this.buttonJoinCLub.Visible = false;
            this.buttonJoinCLub.Click += new System.EventHandler(this.buttonJoinCLub_Click);
            // 
            // buttonLeaveClub
            // 
            this.buttonLeaveClub.Location = new System.Drawing.Point(172, 97);
            this.buttonLeaveClub.Name = "buttonLeaveClub";
            this.buttonLeaveClub.Size = new System.Drawing.Size(75, 23);
            this.buttonLeaveClub.TabIndex = 11;
            this.buttonLeaveClub.Text = "Leave Club";
            this.buttonLeaveClub.UseVisualStyleBackColor = true;
            this.buttonLeaveClub.Visible = false;
            this.buttonLeaveClub.Click += new System.EventHandler(this.buttonLeaveClub_Click);
            // 
            // buttonStartClub
            // 
            this.buttonStartClub.Location = new System.Drawing.Point(253, 97);
            this.buttonStartClub.Name = "buttonStartClub";
            this.buttonStartClub.Size = new System.Drawing.Size(75, 23);
            this.buttonStartClub.TabIndex = 12;
            this.buttonStartClub.Text = "Start a Club";
            this.buttonStartClub.UseVisualStyleBackColor = true;
            this.buttonStartClub.Visible = false;
            this.buttonStartClub.Click += new System.EventHandler(this.buttonStartClub_Click);
            // 
            // buttonModifyClub
            // 
            this.buttonModifyClub.Location = new System.Drawing.Point(171, 97);
            this.buttonModifyClub.Name = "buttonModifyClub";
            this.buttonModifyClub.Size = new System.Drawing.Size(75, 23);
            this.buttonModifyClub.TabIndex = 13;
            this.buttonModifyClub.Text = "Modify Club";
            this.buttonModifyClub.UseVisualStyleBackColor = true;
            this.buttonModifyClub.Visible = false;
            this.buttonModifyClub.Click += new System.EventHandler(this.buttonModifyClub_Click);
            // 
            // buttonAddCardToMyCollection
            // 
            this.buttonAddCardToMyCollection.Location = new System.Drawing.Point(90, 162);
            this.buttonAddCardToMyCollection.Name = "buttonAddCardToMyCollection";
            this.buttonAddCardToMyCollection.Size = new System.Drawing.Size(156, 23);
            this.buttonAddCardToMyCollection.TabIndex = 23;
            this.buttonAddCardToMyCollection.Text = "Add Card To My Collection";
            this.buttonAddCardToMyCollection.UseVisualStyleBackColor = true;
            this.buttonAddCardToMyCollection.Click += new System.EventHandler(this.buttonAddCardToMyCollection_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(169, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Ammount to deposit:";
            // 
            // textBoxDeposit
            // 
            this.textBoxDeposit.Location = new System.Drawing.Point(278, 123);
            this.textBoxDeposit.Name = "textBoxDeposit";
            this.textBoxDeposit.Size = new System.Drawing.Size(50, 20);
            this.textBoxDeposit.TabIndex = 25;
            this.textBoxDeposit.Text = "0";
            this.textBoxDeposit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonDeposit
            // 
            this.buttonDeposit.Location = new System.Drawing.Point(334, 121);
            this.buttonDeposit.Name = "buttonDeposit";
            this.buttonDeposit.Size = new System.Drawing.Size(75, 23);
            this.buttonDeposit.TabIndex = 26;
            this.buttonDeposit.Text = "Deposit";
            this.buttonDeposit.UseVisualStyleBackColor = true;
            this.buttonDeposit.Click += new System.EventHandler(this.buttonDeposit_Click);
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 262);
            this.Controls.Add(this.buttonDeposit);
            this.Controls.Add(this.textBoxDeposit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonAddCardToMyCollection);
            this.Controls.Add(this.buttonModifyClub);
            this.Controls.Add(this.buttonStartClub);
            this.Controls.Add(this.buttonLeaveClub);
            this.Controls.Add(this.buttonJoinCLub);
            this.Controls.Add(this.fundsLabel);
            this.Controls.Add(this.clubLabel);
            this.Controls.Add(this.lastnameLabel);
            this.Controls.Add(this.firstnameLabel);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UserForm";
            this.Text = "UserForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label fundsLabel;
        private System.Windows.Forms.Label clubLabel;
        private System.Windows.Forms.Label lastnameLabel;
        private System.Windows.Forms.Label firstnameLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Button buttonJoinCLub;
        private System.Windows.Forms.Button buttonLeaveClub;
        private System.Windows.Forms.Button buttonStartClub;
        private System.Windows.Forms.Button buttonModifyClub;
        private System.Windows.Forms.Button buttonAddCardToMyCollection;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxDeposit;
        private System.Windows.Forms.Button buttonDeposit;
    }
}