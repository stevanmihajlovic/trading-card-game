﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;


namespace TCG.Forms
{
    public partial class UserForm : Form
    {
        User user;
        public GraphClient client;

        public UserForm()
        {
            InitializeComponent();
        }
        public UserForm (User u, GraphClient cl)
        {
            InitializeComponent();

            client = cl;
            user = u;
            usernameLabel.Text = u.username;
            firstnameLabel.Text = u.firstName;
            lastnameLabel.Text = u.lastName;

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:User), (c:Club), n-[MEMBER]->c where n.id = " + user.id
                                                                          + " return c", new Dictionary<string, object>(), CypherResultMode.Set);
            
            user.club = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList().FirstOrDefault();

            updateInterface();

        }

    private void buttonJoinCLub_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Club) return n;",
                                              new Dictionary<string, object>(), CypherResultMode.Set);
            List<Club> clubs = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList();

            if (clubs.Count == 0)
                MessageBox.Show("There are no Clubs to join!");
            else
            {
                JoinAClubForm join = new JoinAClubForm(client, clubs, user);
                join.ShowDialog();
                updateInterface();
            }

        }

        private void buttonStartClub_Click(object sender, EventArgs e)
        {
            StartAClubForm start = new StartAClubForm(client, user);
            start.ShowDialog();
            updateInterface();
        }

        private void buttonModifyClub_Click(object sender, EventArgs e)
        {
            ModifyClubForm modify = new ModifyClubForm(client, user);
            modify.ShowDialog();
            updateInterface();
        }

        private void updateInterface()
        {
            fundsLabel.Text = user.funds.ToString();
            if (user.isLeader)
            {
                clubLabel.Text = user.club.name;
                buttonModifyClub.Visible = true;
                buttonLeaveClub.Visible = false;
                buttonJoinCLub.Visible = false;
                buttonStartClub.Visible = false;
            }
            else
                if (user.club != null)
                {
                    clubLabel.Text = user.club.name;
                    buttonModifyClub.Visible = false;
                    buttonLeaveClub.Visible = true;
                    buttonJoinCLub.Visible = false;
                    buttonStartClub.Visible = false;
                }
                else
                {
                    clubLabel.Text = "Not a member.";
                    buttonModifyClub.Visible = false;
                    buttonLeaveClub.Visible = false;
                    buttonJoinCLub.Visible = true;
                    buttonStartClub.Visible = true;
                }
        }

        private void buttonLeaveClub_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User), b-[r: MEMBER]->a where b.id = " + user.id
                                                            + " AND a.id = " + user.club.id + " delete r",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);

            user.club = null;

            updateInterface();
        }

        private void buttonAddCardToMyCollection_Click(object sender, EventArgs e)
        {
            AddCardsToCollectionForm addToCollection = new AddCardsToCollectionForm(user, client);
            addToCollection.ShowDialog();
            fundsLabel.Text = user.funds.ToString();
        }

        private void buttonDeposit_Click(object sender, EventArgs e)
        {
            float n;
            try
            {
                if (float.TryParse(textBoxDeposit.Text, out n) && n > 0)
                {
                        //update user funds
                        user.funds+=n;

                        var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id = " + user.id + " set n.funds = " + user.funds
                                                                            + " return n", new Dictionary<string, object>(), CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query2).ToList();
                        
                        fundsLabel.Text = user.funds.ToString();

                        MessageBox.Show("Money successfully deposited!");
                }
                else
                    throw new Exception("Please enter only non-negative numbers for deposit!");
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
    }
}
