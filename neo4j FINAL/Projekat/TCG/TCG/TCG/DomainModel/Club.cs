﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCG.DomainModel
{
    public class Club
    {
        public String id { get; set; }
        public String name { get; set; }
        public User leader { get; set; }
        public List<User> members { get; set; }
        public float funds { get; set; }
        public float discount { get; set; }
        public float monthlyFee { get; set; }

        public Club()
        {
            members = new List<User>();
        }
    }
}
