﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCG.DomainModel
{
    public class MonsterCard
    {
        public int id { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String condition { get; set; }
        public String rarity { get; set; }
        public String type { get; set; } //enum?
        public String attribute { get; set; } //enum (dark, light, water, fire, earth, wind)
        public int level { get; set; } //1-12
        public int attack { get; set; }
        public int defense { get; set; }
        public float value { get; set; }

        public MonsterCard(){}
    }
}
