﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCG.DomainModel
{
    public class RelMember
    {
        public User user { get; set; }
        public Club club { get; set; }
        public bool isLeader { get; set; }
        public RelMember()
        {

        }
    }
}
