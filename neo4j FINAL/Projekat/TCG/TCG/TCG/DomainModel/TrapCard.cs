﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCG.DomainModel
{
    public class TrapCard
    {
        public int id { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String condition { get; set; }
        public String rarity { get; set; }
        public String icon { get; set; } //enum (normal, continuous, counter)
        public float value { get; set; }
        public TrapCard(){}
    }
}
