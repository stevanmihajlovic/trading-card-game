﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCG.DomainModel
{
    public class User
    {
        public String id { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public Club club { get; set; }
        public bool isLeader { get; set; }
        public List<MonsterCard> monsterCards { get; set; }
        public List<SpellCard> spellCards { get; set; }
        public List<TrapCard> trapCards { get; set; }
        public float funds { get; set; }

        public User()
        {
            monsterCards = new List<MonsterCard>();
            spellCards = new List<SpellCard>();
            trapCards = new List<TrapCard>();
        }
    }
}
