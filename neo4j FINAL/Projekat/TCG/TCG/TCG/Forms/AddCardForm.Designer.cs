﻿namespace TCG.Forms
{
    partial class AddCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.comboBoxCondition = new System.Windows.Forms.ComboBox();
            this.groupBoxMonsterCard = new System.Windows.Forms.GroupBox();
            this.textBoxMonsterDefense = new System.Windows.Forms.TextBox();
            this.textBoxMonsterAttack = new System.Windows.Forms.TextBox();
            this.comboBoxMonsterLevel = new System.Windows.Forms.ComboBox();
            this.comboBoxMonsterAttribute = new System.Windows.Forms.ComboBox();
            this.comboBoxMonsterType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxSpellCard = new System.Windows.Forms.GroupBox();
            this.comboBoxSpellIcon = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBoxTrapCard = new System.Windows.Forms.GroupBox();
            this.comboBoxTrapIcon = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxRarity = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonAddCard = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.groupBoxMonsterCard.SuspendLayout();
            this.groupBoxSpellCard.SuspendLayout();
            this.groupBoxTrapCard.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Condition";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(112, 20);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(188, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(112, 74);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(188, 72);
            this.textBoxDescription.TabIndex = 4;
            // 
            // comboBoxCondition
            // 
            this.comboBoxCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCondition.FormattingEnabled = true;
            this.comboBoxCondition.Items.AddRange(new object[] {
            "Near Mint",
            "Used",
            "Damaged"});
            this.comboBoxCondition.Location = new System.Drawing.Point(112, 47);
            this.comboBoxCondition.Name = "comboBoxCondition";
            this.comboBoxCondition.Size = new System.Drawing.Size(188, 21);
            this.comboBoxCondition.TabIndex = 5;
            // 
            // groupBoxMonsterCard
            // 
            this.groupBoxMonsterCard.Controls.Add(this.textBoxMonsterDefense);
            this.groupBoxMonsterCard.Controls.Add(this.textBoxMonsterAttack);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterLevel);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterAttribute);
            this.groupBoxMonsterCard.Controls.Add(this.comboBoxMonsterType);
            this.groupBoxMonsterCard.Controls.Add(this.label10);
            this.groupBoxMonsterCard.Controls.Add(this.label9);
            this.groupBoxMonsterCard.Controls.Add(this.label8);
            this.groupBoxMonsterCard.Controls.Add(this.label7);
            this.groupBoxMonsterCard.Controls.Add(this.label4);
            this.groupBoxMonsterCard.Location = new System.Drawing.Point(12, 257);
            this.groupBoxMonsterCard.Name = "groupBoxMonsterCard";
            this.groupBoxMonsterCard.Size = new System.Drawing.Size(306, 150);
            this.groupBoxMonsterCard.TabIndex = 6;
            this.groupBoxMonsterCard.TabStop = false;
            this.groupBoxMonsterCard.Text = "Additional Information:";
            this.groupBoxMonsterCard.Visible = false;
            // 
            // textBoxMonsterDefense
            // 
            this.textBoxMonsterDefense.Location = new System.Drawing.Point(112, 125);
            this.textBoxMonsterDefense.Name = "textBoxMonsterDefense";
            this.textBoxMonsterDefense.Size = new System.Drawing.Size(188, 20);
            this.textBoxMonsterDefense.TabIndex = 17;
            this.textBoxMonsterDefense.Text = "0";
            this.textBoxMonsterDefense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxMonsterAttack
            // 
            this.textBoxMonsterAttack.Location = new System.Drawing.Point(112, 99);
            this.textBoxMonsterAttack.Name = "textBoxMonsterAttack";
            this.textBoxMonsterAttack.Size = new System.Drawing.Size(188, 20);
            this.textBoxMonsterAttack.TabIndex = 16;
            this.textBoxMonsterAttack.Text = "0";
            this.textBoxMonsterAttack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBoxMonsterLevel
            // 
            this.comboBoxMonsterLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterLevel.FormattingEnabled = true;
            this.comboBoxMonsterLevel.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxMonsterLevel.Location = new System.Drawing.Point(112, 71);
            this.comboBoxMonsterLevel.Name = "comboBoxMonsterLevel";
            this.comboBoxMonsterLevel.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterLevel.TabIndex = 13;
            // 
            // comboBoxMonsterAttribute
            // 
            this.comboBoxMonsterAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterAttribute.FormattingEnabled = true;
            this.comboBoxMonsterAttribute.Items.AddRange(new object[] {
            "Dark",
            "Divine",
            "Earth",
            "Fire",
            "Light",
            "Water",
            "Wind"});
            this.comboBoxMonsterAttribute.Location = new System.Drawing.Point(112, 44);
            this.comboBoxMonsterAttribute.Name = "comboBoxMonsterAttribute";
            this.comboBoxMonsterAttribute.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterAttribute.TabIndex = 12;
            // 
            // comboBoxMonsterType
            // 
            this.comboBoxMonsterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMonsterType.FormattingEnabled = true;
            this.comboBoxMonsterType.Items.AddRange(new object[] {
            "Aqua",
            "Beast",
            "Beast-Warrior",
            "Creator God",
            "Dinosaur",
            "Divine-Beast",
            "Dragon",
            "Fairy",
            "Fiend",
            "Fish",
            "Insect",
            "Machine",
            "Plant",
            "Psychic",
            "Pyro",
            "Reptile",
            "Rock",
            "Sea Serpent",
            "Spellcaster",
            "Thunder",
            "Warrior",
            "Winged Beast",
            "Zombie"});
            this.comboBoxMonsterType.Location = new System.Drawing.Point(112, 18);
            this.comboBoxMonsterType.Name = "comboBoxMonsterType";
            this.comboBoxMonsterType.Size = new System.Drawing.Size(188, 21);
            this.comboBoxMonsterType.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Defense:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Attack:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Level:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Attribute:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Type:";
            // 
            // groupBoxSpellCard
            // 
            this.groupBoxSpellCard.Controls.Add(this.comboBoxSpellIcon);
            this.groupBoxSpellCard.Controls.Add(this.label5);
            this.groupBoxSpellCard.Location = new System.Drawing.Point(15, 257);
            this.groupBoxSpellCard.Name = "groupBoxSpellCard";
            this.groupBoxSpellCard.Size = new System.Drawing.Size(303, 44);
            this.groupBoxSpellCard.TabIndex = 7;
            this.groupBoxSpellCard.TabStop = false;
            this.groupBoxSpellCard.Text = "Additional Information:";
            this.groupBoxSpellCard.Visible = false;
            // 
            // comboBoxSpellIcon
            // 
            this.comboBoxSpellIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpellIcon.FormattingEnabled = true;
            this.comboBoxSpellIcon.Items.AddRange(new object[] {
            "Continuous",
            "Equip",
            "Field",
            "Normal",
            "Ritual",
            "Quick Play"});
            this.comboBoxSpellIcon.Location = new System.Drawing.Point(109, 16);
            this.comboBoxSpellIcon.Name = "comboBoxSpellIcon";
            this.comboBoxSpellIcon.Size = new System.Drawing.Size(188, 21);
            this.comboBoxSpellIcon.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Icon:";
            // 
            // groupBoxTrapCard
            // 
            this.groupBoxTrapCard.Controls.Add(this.comboBoxTrapIcon);
            this.groupBoxTrapCard.Controls.Add(this.label6);
            this.groupBoxTrapCard.Location = new System.Drawing.Point(15, 257);
            this.groupBoxTrapCard.Name = "groupBoxTrapCard";
            this.groupBoxTrapCard.Size = new System.Drawing.Size(303, 44);
            this.groupBoxTrapCard.TabIndex = 8;
            this.groupBoxTrapCard.TabStop = false;
            this.groupBoxTrapCard.Text = "Additional Information:";
            this.groupBoxTrapCard.Visible = false;
            // 
            // comboBoxTrapIcon
            // 
            this.comboBoxTrapIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTrapIcon.FormattingEnabled = true;
            this.comboBoxTrapIcon.Items.AddRange(new object[] {
            "Continous",
            "Counter",
            "Normal"});
            this.comboBoxTrapIcon.Location = new System.Drawing.Point(109, 15);
            this.comboBoxTrapIcon.Name = "comboBoxTrapIcon";
            this.comboBoxTrapIcon.Size = new System.Drawing.Size(188, 21);
            this.comboBoxTrapIcon.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Icon:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Rarity: ";
            // 
            // comboBoxRarity
            // 
            this.comboBoxRarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRarity.FormattingEnabled = true;
            this.comboBoxRarity.Items.AddRange(new object[] {
            "Common",
            "Rare",
            "Super Rare",
            "Ultra Rare",
            "Secret Rare",
            "Ultimate Rare",
            "Gold Rare",
            "Ghost Rare"});
            this.comboBoxRarity.Location = new System.Drawing.Point(112, 160);
            this.comboBoxRarity.Name = "comboBoxRarity";
            this.comboBoxRarity.Size = new System.Drawing.Size(188, 21);
            this.comboBoxRarity.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxValue);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBoxRarity);
            this.groupBox1.Controls.Add(this.comboBoxCondition);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Controls.Add(this.textBoxDescription);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 225);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Information:";
            // 
            // buttonAddCard
            // 
            this.buttonAddCard.AllowDrop = true;
            this.buttonAddCard.Location = new System.Drawing.Point(230, 427);
            this.buttonAddCard.Name = "buttonAddCard";
            this.buttonAddCard.Size = new System.Drawing.Size(88, 23);
            this.buttonAddCard.TabIndex = 12;
            this.buttonAddCard.Text = "Add Card";
            this.buttonAddCard.UseVisualStyleBackColor = true;
            this.buttonAddCard.Click += new System.EventHandler(this.buttonAddCard_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 194);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Value";
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(112, 194);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(188, 20);
            this.textBoxValue.TabIndex = 12;
            this.textBoxValue.Text = "1";
            this.textBoxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AddCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 471);
            this.Controls.Add(this.buttonAddCard);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxTrapCard);
            this.Controls.Add(this.groupBoxSpellCard);
            this.Controls.Add(this.groupBoxMonsterCard);
            this.Name = "AddCardForm";
            this.Text = "AddCard";
            this.groupBoxMonsterCard.ResumeLayout(false);
            this.groupBoxMonsterCard.PerformLayout();
            this.groupBoxSpellCard.ResumeLayout(false);
            this.groupBoxSpellCard.PerformLayout();
            this.groupBoxTrapCard.ResumeLayout(false);
            this.groupBoxTrapCard.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.ComboBox comboBoxCondition;
        private System.Windows.Forms.GroupBox groupBoxMonsterCard;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxSpellCard;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBoxTrapCard;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxMonsterDefense;
        private System.Windows.Forms.TextBox textBoxMonsterAttack;
        private System.Windows.Forms.ComboBox comboBoxMonsterLevel;
        private System.Windows.Forms.ComboBox comboBoxMonsterAttribute;
        private System.Windows.Forms.ComboBox comboBoxMonsterType;
        private System.Windows.Forms.ComboBox comboBoxSpellIcon;
        private System.Windows.Forms.ComboBox comboBoxTrapIcon;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxRarity;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonAddCard;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.Label label12;
    }
}