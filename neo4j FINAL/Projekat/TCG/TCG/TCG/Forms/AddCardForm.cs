﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class AddCardForm : Form
    {
        public GraphClient client;
        private int type;
        public AddCardForm()
        {
            InitializeComponent();
        }
        public AddCardForm(int a)
        {
            type = a;
            InitializeComponent();
            comboBoxCondition.SelectedIndex = 0;    //promeni na normal sve
            comboBoxRarity.SelectedIndex = 0;
            if (type == 1)
            {
                comboBoxMonsterType.SelectedIndex = 0;
                comboBoxMonsterAttribute.SelectedIndex = 0;
                comboBoxMonsterLevel.SelectedIndex = 0;
                groupBoxMonsterCard.Visible = true;

            }
            if (type == 2)
            {
                groupBoxSpellCard.Visible = true;
                comboBoxSpellIcon.SelectedIndex = 0;
            }
            if (type == 3)
            {
                groupBoxTrapCard.Visible = true;
                comboBoxTrapIcon.SelectedIndex = 0;
            }

        }

        private void buttonAddCard_Click(object sender, EventArgs e)
        {
            if (type == 1)
            {
                try
                {
                    MonsterCard monsterCard = createMonsterCard();

                    string maxId = getMaxId("MonsterCard");

                    try
                    {
                        int mId = int.Parse(maxId);
                        monsterCard.id = (++mId);
                    }
                    catch (Exception exception)
                    {
                        monsterCard.id = 1;
                    }


                    Dictionary<string, object> queryDict = new Dictionary<string, object>();
                    queryDict.Add("name", monsterCard.name);
                    queryDict.Add("description", monsterCard.description);
                    queryDict.Add("condition", monsterCard.condition);
                    queryDict.Add("rarity", monsterCard.rarity);
                    queryDict.Add("value", monsterCard.value);
                    queryDict.Add("type", monsterCard.type);
                    queryDict.Add("attribute", monsterCard.attribute);
                    queryDict.Add("level", monsterCard.level);
                    queryDict.Add("attack", monsterCard.attack);
                    queryDict.Add("defense", monsterCard.defense);
                    queryDict.Add("__type__", "org.neo4j.TCG.domain.MonsterCard");  //karte mogu da imaju isto ime ali razlicit raritet 
                                                                                    //sta raditi sa kartama u razlicitom condition

                    var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:MonsterCard {id:" + monsterCard.id + ", name:'" + monsterCard.name
                                                                    + "', description:'" + monsterCard.description + "', condition:'" + monsterCard.condition
                                                                    + "', rarity:'" + monsterCard.rarity + "', value:'" + monsterCard.value
                                                                    + "', type:'" + monsterCard.type
                                                                    + "', attribute:'" + monsterCard.attribute + "', level:'" + monsterCard.level
                                                                    + "', attack:'" + monsterCard.attack + "', defense:'" + monsterCard.defense
                                                                    + "', __type__:'org.neo4j.TCG.domain.MonsterCard'}) return n",
                                                                    queryDict, CypherResultMode.Set);

                    List<MonsterCard> monsterCards = ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();//da li je ovo potrebno?
                    MessageBox.Show("Successfully added Monster card!");
                    Close();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }

            if (type == 2)
            {
                try
                {
                    SpellCard spellCard = createSpellCard();

                    string maxId = getMaxId("SpellCard");

                    try
                    {
                        int mId = int.Parse(maxId);
                        spellCard.id = (++mId);
                    }
                    catch (Exception exception)
                    {
                        spellCard.id = 1;
                    }


                    Dictionary<string, object> queryDict = new Dictionary<string, object>();
                    queryDict.Add("name", spellCard.name);
                    queryDict.Add("description", spellCard.description);
                    queryDict.Add("condition", spellCard.condition);
                    queryDict.Add("rarity", spellCard.rarity);
                    queryDict.Add("value", spellCard.value);
                    queryDict.Add("icon", spellCard.icon);
                    queryDict.Add("__type__", "org.neo4j.TCG.domain.SpellCard");

                    var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:SpellCard {id:" + spellCard.id + ", name:'" + spellCard.name
                                                                    + "', description:'" + spellCard.description + "', condition:'" + spellCard.condition
                                                                    + "', rarity:'" + spellCard.rarity + "', value:'" + spellCard.value
                                                                    + "', icon:'" + spellCard.icon
                                                                    + "', __type__:'org.neo4j.TCG.domain.SpellCard'}) return n",
                                                                    queryDict, CypherResultMode.Set);

                    List<SpellCard> spellCards = ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();
                    MessageBox.Show("Successfully added Spell card!");
                    Close();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
            if (type == 3)
            { 
                try
                {
                    TrapCard trapCard = createTrapCard();

                    string maxId = getMaxId("TrapCard");

                    try
                    {
                        int mId = int.Parse(maxId);
                        trapCard.id = (++mId);
                    }
                    catch (Exception exception)
                    {
                        trapCard.id = 1;
                    }


                    Dictionary<string, object> queryDict = new Dictionary<string, object>();
                    queryDict.Add("name", trapCard.name);
                    queryDict.Add("description", trapCard.description);
                    queryDict.Add("condition", trapCard.condition);
                    queryDict.Add("rarity", trapCard.rarity);
                    queryDict.Add("value", trapCard.value);
                    queryDict.Add("icon", trapCard.icon);
                    queryDict.Add("__type__", "org.neo4j.TCG.domain.TrapCard");

                    var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:TrapCard {id:" + trapCard.id + ", name:'" + trapCard.name
                                                                    + "', description:'" + trapCard.description + "', condition:'" + trapCard.condition
                                                                    + "', rarity:'" + trapCard.rarity + "', value:'" + trapCard.value
                                                                    + "', icon:'" + trapCard.icon
                                                                    + "', __type__:'org.neo4j.TCG.domain.TrapCard'}) return n",
                                                                    queryDict, CypherResultMode.Set);

                    List<TrapCard> trapCards = ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();
                    MessageBox.Show("Successfully added Trap card!");
                    Close();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private TrapCard createTrapCard()
        {
            TrapCard a = new TrapCard();
            a.name = textBoxName.Text;
            if (a.name == "")
                throw new Exception("Please enter card name!");
            a.description = textBoxDescription.Text;
            if (a.description == "")
                throw new Exception("Please enter card description!");
            a.condition = comboBoxCondition.SelectedItem.ToString();
            a.rarity = comboBoxRarity.SelectedItem.ToString();
            float m;
            if (float.TryParse(textBoxValue.Text, out m))
            {
                a.value = m;
                textBoxValue.BackColor = Color.White;
            }
            else
            {
                textBoxValue.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for value!");
            }
            a.icon = comboBoxTrapIcon.SelectedItem.ToString();
            return a;
        }

        private SpellCard createSpellCard()
        {
            SpellCard a = new SpellCard();
            a.name = textBoxName.Text;
            if (a.name == "")
                throw new Exception("Please enter card name!");
            a.description = textBoxDescription.Text;
            if (a.description == "")
                throw new Exception("Please enter card description!");
            a.condition = comboBoxCondition.SelectedItem.ToString();
            a.rarity = comboBoxRarity.SelectedItem.ToString();
            float m;
            if (float.TryParse(textBoxValue.Text, out m))
            {
                a.value = m;
                textBoxValue.BackColor = Color.White;
            }
            else
            {
                textBoxValue.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for value!");
            }
            a.icon = comboBoxSpellIcon.SelectedItem.ToString();
            return a;
        }

        private MonsterCard createMonsterCard()
        {      
            MonsterCard a = new MonsterCard();
            a.name = textBoxName.Text;
            if (a.name == "")
                throw new Exception("Please enter card name!");
            a.description = textBoxDescription.Text;
            if (a.description == "")
                throw new Exception("Please enter card description!");
            a.condition = comboBoxCondition.SelectedItem.ToString();
            a.rarity = comboBoxRarity.SelectedItem.ToString();
            float m;
            if (float.TryParse(textBoxValue.Text, out m))
            {
                a.value = m;
                textBoxValue.BackColor = Color.White;
            }
            else
            {
                textBoxValue.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for value!");
            }
            a.type = comboBoxMonsterType.SelectedItem.ToString();
            a.attribute = comboBoxMonsterAttribute.SelectedItem.ToString();           
            a.level = Convert.ToInt32(comboBoxMonsterLevel.SelectedItem.ToString());
            int n;
            if (int.TryParse(textBoxMonsterAttack.Text, out n))
            {
                a.attack = n;
                textBoxMonsterAttack.BackColor = Color.White;
            }
            else
            {
                textBoxMonsterAttack.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for attack!");
            }
            if (int.TryParse(textBoxMonsterDefense.Text, out n))
                a.defense = n;
            else
            {
                textBoxMonsterDefense.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for defense!");                
            }
            return a;
        }

        private String getMaxId(string s)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:"+s+") where has(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
