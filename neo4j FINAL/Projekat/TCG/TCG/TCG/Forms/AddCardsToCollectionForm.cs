﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class AddCardsToCollectionForm : Form
    {
        private User user;
        private GraphClient client;
        private string cardType;
        private string queryText;
        private List<MonsterCard> monsterCards;
        private List<SpellCard> spellCards;
        private List<TrapCard> trapCards;
        private List<int> ids;

        public AddCardsToCollectionForm(User u, GraphClient cl)
        {
            InitializeComponent();
            user = u;
            client = cl;
            cardType = "";
            queryText = "";
            ids = new List<int>();
            monsterCards = new List<MonsterCard>();
            spellCards = new List<SpellCard>();
            trapCards = new List<TrapCard>();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            ids.Clear();
            monsterCards.Clear();
            spellCards.Clear();
            trapCards.Clear();
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            if (comboBoxType.SelectedItem.Equals("MonsterCard"))
            {
                commonQueryProperties();
                if (comboBoxMonsterType.SelectedIndex != -1)
                    queryText += " and a.type = '" + comboBoxMonsterType.SelectedItem.ToString() + "'";
                if (comboBoxMonsterAttribute.SelectedIndex != -1)
                    queryText += " and a.attribute = '" + comboBoxMonsterAttribute.SelectedItem.ToString() + "'";
                if (comboBoxMonsterLevel.SelectedIndex != -1)
                    queryText += " and a.level = '" + comboBoxMonsterLevel.SelectedItem.ToString() + "'";
                int m;
                if (int.TryParse(textBoxFromAttack.Text, out m))
                    queryText += " and a.attack >= '" + m + "'";
                if (int.TryParse(textBoxToAttack.Text, out m))
                    queryText += " and a.attack <= '" + m + "'";
                if (int.TryParse(textBoxFromDefense.Text, out m))
                    queryText += " and a.defense >= '" + m + "'";
                if (int.TryParse(textBoxToDefense.Text, out m))
                    queryText += " and a.defense <= '" + m + "'";

                queryText += " and not b-[:OWNS]->a return a";//Showing only cards which user does not own

                var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                monsterCards = ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();

                foreach (MonsterCard mC in monsterCards)
                {
                    listBox1.Items.Add(mC.name);
                    ids.Add(Convert.ToInt32(mC.id));
                }
            }
            else
                if (comboBoxType.SelectedItem.Equals("SpellCard"))
                {
                    commonQueryProperties();
                    if (comboBoxSpellIcon.SelectedIndex != -1)
                        queryText += " and a.icon = '" + comboBoxSpellIcon.SelectedItem.ToString() + "'";
                    queryText += " and not b-[:OWNS]->a return a";//Showing only cards which user does not own

                    var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                    spellCards = ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();

                    foreach (SpellCard sC in spellCards)
                    {
                        listBox1.Items.Add(sC.name);
                        ids.Add(Convert.ToInt32(sC.id));
                    }
                }
                else
                    if (comboBoxType.SelectedItem.Equals("TrapCard"))
                    {
                        commonQueryProperties();
                        if (comboBoxTrapIcon.SelectedIndex != -1)
                            queryText += " and a.icon = '" + comboBoxTrapIcon.SelectedItem.ToString() + "'";
                        queryText += " and not b-[:OWNS]->a return a";//Showing only cards which user does not own

                        var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                        trapCards = ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();

                        foreach (TrapCard tC in trapCards)
                        {
                            listBox1.Items.Add(tC.name);
                            ids.Add(Convert.ToInt32(tC.id));
                        }
                    }
                    else
                        MessageBox.Show("Please select type of card first!");

        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (comboBoxType.SelectedItem.Equals("MonsterCard"))
            {
                cardType = "MonsterCard";
                groupBoxMonsterCard.Visible = true;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("SpellCard"))
            {
                cardType = "SpellCard";
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = true;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("TrapCard"))
            {
                cardType = "TrapCard";
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = true;
            }
        }

        private void commonQueryProperties()
        {
            float m;
            queryText = "match (a:" + cardType + "), (b:User) where b.id = '" + user.id + "'";
            
            if (textBoxName.Text != "")
                queryText += " and a.name = '" + textBoxName.Text + "'";
            if (comboBoxCondition.SelectedIndex != -1)
                queryText += " and a.condition = '" + comboBoxCondition.SelectedItem.ToString() + "'";
            if (comboBoxRarity.SelectedIndex != -1)
                queryText += " and a.rarity = '" + comboBoxRarity.SelectedItem.ToString() + "'";

            if (float.TryParse(textBoxFromValue.Text, out m))
                queryText += " and a.value >= '" + m + "'";
            if (float.TryParse(textBoxToValue.Text, out m))
                queryText += " and a.value <= '" + m + "'";
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Please select a card you wish to add first!");
            else
            {
                int m;
                if (int.TryParse(textBoxQuantity.Text, out m))
                    if (m < 1)
                    {
                        MessageBox.Show("Please enter number larger than zero (0) for quantity!");
                        textBoxQuantity.BackColor = Color.Red;
                    }
                    else
                    {
                        textBoxQuantity.BackColor = Color.White;
                        var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User), (b:" + cardType + ") WHERE a.id =~ '" + user.id
                                                                       + "' AND b.id =~ '" + ids[listBox1.SelectedIndex]
                                                                       + "' CREATE(a) -[r: OWNS{ quantity : '" + m + "' }]->(b) RETURN r",
                                                                       new Dictionary<string, object>(),
                                                                       CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<RelMember>(query).ToList();

                        ids.RemoveAt(listBox1.SelectedIndex);
                        listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                        MessageBox.Show("Succesfully added card to your Collection!");
                    }
                else
                {
                    textBoxQuantity.BackColor = Color.Red;
                    MessageBox.Show("Please enter only numbers for value!");
                }
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
