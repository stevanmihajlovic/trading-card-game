﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace TCG.Forms
{
    public partial class AdminAccessForm : Form
    {
        public GraphClient client;
        public AdminAccessForm()
        {
            InitializeComponent();
        }

        private void buttonAddMonsterCard_Click(object sender, EventArgs e)
        {
            addCard(1);
        }

        private void buttonAddSpellCard_Click(object sender, EventArgs e)
        {
            addCard(2);
        }

        private void buttonAddTrapCard_Click(object sender, EventArgs e)
        {
            addCard(3);
        }
        private void addCard(int a)
        {
            AddCardForm reg = new AddCardForm(a);
            reg.client = this.client;
            reg.ShowDialog();
        }

        private void buttonModifyMonsterCard_Click(object sender, EventArgs e)
        {
            modifyCard("MonsterCard");
        }

        private void buttonModifySpellCard_Click(object sender, EventArgs e)
        {
            modifyCard("SpellCard");
        }

        private void buttonModifyTrapCard_Click(object sender, EventArgs e)
        {
            modifyCard("TrapCard");
        }
        private void modifyCard(string a)
        {
            ModifyCardForm reg = new ModifyCardForm(a,this.client);
            reg.ShowDialog();
        }
    }
}
