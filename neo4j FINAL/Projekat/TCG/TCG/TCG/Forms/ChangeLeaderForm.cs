﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class ChangeLeaderForm : Form
    {
        private GraphClient client;
        private User user;
        private Club club;
        private List<User> users;

        public ChangeLeaderForm(GraphClient cl, User us, Club clu, List<User> use)
        {
            client = cl;
            user = us;
            club = clu;
            users = use;
            InitializeComponent();            
            
            foreach (User u in users)
                listBox1.Items.Add(u.username);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            User newLeader = users[listBox1.SelectedIndex];
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Club), (m:User), m-[r:MEMBER]->n where m-[:MEMBER]->n and m.id =~ '" + newLeader.id
                                                          + "' set r.isLeader = 'true', m.isLeader = 'true' return m",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);

            var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Club), (m:User), m-[r:MEMBER]->n where m-[:MEMBER]->n and m.id =~ '" + user.id
                                                          + "' set r.isLeader = 'false', m.isLeader = 'false' return m",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query1);
            user.isLeader = false;
            MessageBox.Show("You have appointed new leader for your Club!");
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
