﻿namespace TCG.Forms
{
    partial class JoinAClubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.ClubName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NumberOfMembers = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Discount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MonthlyFee = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonJoinClub = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ClubName,
            this.NumberOfMembers,
            this.Discount,
            this.MonthlyFee});
            this.listView1.Location = new System.Drawing.Point(30, 45);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(381, 313);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // ClubName
            // 
            this.ClubName.Text = "Club Name";
            this.ClubName.Width = 106;
            // 
            // NumberOfMembers
            // 
            this.NumberOfMembers.Text = "Number of Members";
            this.NumberOfMembers.Width = 117;
            // 
            // Discount
            // 
            this.Discount.Text = "Discount";
            // 
            // MonthlyFee
            // 
            this.MonthlyFee.Text = "Monthly Fee";
            this.MonthlyFee.Width = 89;
            // 
            // buttonJoinClub
            // 
            this.buttonJoinClub.Location = new System.Drawing.Point(255, 382);
            this.buttonJoinClub.Name = "buttonJoinClub";
            this.buttonJoinClub.Size = new System.Drawing.Size(75, 23);
            this.buttonJoinClub.TabIndex = 2;
            this.buttonJoinClub.Text = "Join Club";
            this.buttonJoinClub.UseVisualStyleBackColor = true;
            this.buttonJoinClub.Click += new System.EventHandler(this.buttonJoinClub_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(336, 382);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click_1);
            // 
            // JoinAClubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 443);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonJoinClub);
            this.Controls.Add(this.listView1);
            this.Name = "JoinAClubForm";
            this.Text = "JoinAClubForm";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ClubName;
        private System.Windows.Forms.ColumnHeader NumberOfMembers;
        private System.Windows.Forms.ColumnHeader Discount;
        private System.Windows.Forms.ColumnHeader MonthlyFee;
        private System.Windows.Forms.Button buttonJoinClub;
        private System.Windows.Forms.Button buttonCancel;
    }
}