﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class KickUserForm : Form
    {
        private GraphClient client;
        private User user;
        private List<User> users;

        public KickUserForm(User us, GraphClient cl)
        {
            InitializeComponent();
            client = cl;
            user = us;

            var query = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User), b-[r: MEMBER]->a where b.id <> '" + user.id
                                                            + "' AND a.id = '" + user.club.id + "' return b",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
            users = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();
            
            foreach(User u in users)
            {
                listBox1.Items.Add(u.username);
            }
        }

        private void buttonKick_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Please select a player you wish to kick first!");
            else
            {
                if (user.club.funds >= user.club.monthlyFee)
                {
                    var query = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User), b-[r: MEMBER]->a where b.id = '" + users[listBox1.SelectedIndex].id
                                                                + "' AND a.id = '" + user.club.id + "' delete r",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);
                    ((IRawGraphClient)client).ExecuteCypher(query);


                    //add money to user
                    float temp = users[listBox1.SelectedIndex].funds + user.club.monthlyFee;
                    var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id =~ '" + users[listBox1.SelectedIndex].id
                                                                    + "' set n.funds = '" + temp
                                                                    + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query1).ToList();

                    //deduct money from club
                    temp = user.club.funds - user.club.monthlyFee;
                    var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id =~ '" + user.club.id + "' set n.funds = '" + temp
                                                                   + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query2).ToList();

                    user.club.funds = temp;

                    MessageBox.Show("You have successfully kicked player " + users[listBox1.SelectedIndex].username + " from your club!");

                    users.RemoveAt(listBox1.SelectedIndex);
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);                    
                }
                else
                    MessageBox.Show("Insufficient funds, you need to repay one monthly fee when kicking a member!");
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}