﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class ModifyCardForm : Form
    {
        public GraphClient client;
        private string type;
        List<MonsterCard> monsterCards;
        List<SpellCard> spellCards;
        List<TrapCard> trapCards;
        public ModifyCardForm()
        {
            InitializeComponent();
        }
        public ModifyCardForm(string a, GraphClient c)
        {
            client = c;
            type = a;
            InitializeComponent();
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:"+type+") return n",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            if (type.Equals("MonsterCard"))
            {
                monsterCards = new List<MonsterCard>();
                monsterCards = ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();
                foreach (MonsterCard n in monsterCards)
                    listBox1.Items.Add(n.name);
                groupBoxMonsterCard.Visible = true;
            }
            if (type.Equals("SpellCard"))
            {
                spellCards = new List<SpellCard>();
                spellCards = ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();
                foreach (SpellCard n in spellCards)
                    listBox1.Items.Add(n.name);
                groupBoxSpellCard.Visible = true;
            }
            if (type.Equals("TrapCard"))
            {
                trapCards = new List<TrapCard>();
                trapCards = ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();
                foreach (TrapCard n in trapCards)
                    listBox1.Items.Add(n.name);
                groupBoxTrapCard.Visible = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                return;
            if (type.Equals("MonsterCard"))
            {
                MonsterCard selected = monsterCards[listBox1.SelectedIndex];

                textBoxName.Text = selected.name;
                textBoxDescription.Text = selected.description;
                comboBoxCondition.SelectedIndex = comboBoxCondition.FindStringExact(selected.condition);
                comboBoxRarity.SelectedIndex = comboBoxRarity.FindStringExact(selected.rarity);
                textBoxValue.Text = selected.value.ToString();

                comboBoxMonsterType.SelectedIndex = comboBoxMonsterType.FindStringExact(selected.type);
                comboBoxMonsterAttribute.SelectedIndex = comboBoxMonsterAttribute.FindStringExact(selected.attribute);
                comboBoxMonsterLevel.SelectedIndex = comboBoxMonsterLevel.FindStringExact(selected.level.ToString());
                textBoxMonsterAttack.Text = selected.attack.ToString();
                textBoxMonsterDefense.Text = selected.defense.ToString();
            }

            if (type.Equals("SpellCard"))
            {
                SpellCard selected = spellCards[listBox1.SelectedIndex];

                textBoxName.Text = selected.name;
                textBoxDescription.Text = selected.description;
                comboBoxCondition.SelectedIndex = comboBoxCondition.FindStringExact(selected.condition);
                comboBoxRarity.SelectedIndex = comboBoxRarity.FindStringExact(selected.rarity);
                textBoxValue.Text = selected.value.ToString();

                comboBoxSpellIcon.SelectedIndex = comboBoxSpellIcon.FindStringExact(selected.icon);
            }

            if (type.Equals("TrapCard"))
            {
                TrapCard selected = trapCards[listBox1.SelectedIndex];

                textBoxName.Text = selected.name;
                textBoxDescription.Text = selected.description;
                comboBoxCondition.SelectedIndex = comboBoxCondition.FindStringExact(selected.condition);
                comboBoxRarity.SelectedIndex = comboBoxRarity.FindStringExact(selected.rarity);
                textBoxValue.Text = selected.value.ToString();

                comboBoxTrapIcon.SelectedIndex = comboBoxTrapIcon.FindStringExact(selected.icon);
            }
        }

        private void modifyMonsterCard()
        {
            MonsterCard a = monsterCards[listBox1.SelectedIndex];
            a.name = textBoxName.Text;
            if (a.name == "")
                throw new Exception("Please enter card name!");

            a.description = textBoxDescription.Text;
            if (a.description == "")
                throw new Exception("Please enter card description!");
            a.condition = comboBoxCondition.SelectedItem.ToString();
            a.rarity = comboBoxRarity.SelectedItem.ToString();
            float m;
            if (float.TryParse(textBoxValue.Text, out m))
            {
                a.value = m;
                textBoxValue.BackColor = Color.White;
            }
            else
            {
                textBoxValue.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for value!");
            }
            a.type = comboBoxMonsterType.SelectedItem.ToString();
            a.attribute = comboBoxMonsterAttribute.SelectedItem.ToString();
            a.level = Convert.ToInt32(comboBoxMonsterLevel.SelectedItem.ToString());
            int n;
            if (int.TryParse(textBoxMonsterAttack.Text, out n))
                a.attack = n;
            else
            {
                textBoxMonsterAttack.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for attack!");
            }
            if (int.TryParse(textBoxMonsterDefense.Text, out n))
                a.defense = n;
            else
            {
                textBoxMonsterDefense.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for defense!");
            }

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:"+type+") where n.id =~ '" + a.id + "' set n.name = '" + a.name
                                                                    + "', n.description='" + a.description + "', n.condition='" + a.condition
                                                                    + "', n.rarity='" + a.rarity + "', n.value='" + a.value
                                                                    + "', n.type='" + a.type
                                                                    + "', n.attribute='" + a.attribute + "', n.level='" + a.level
                                                                    + "', n.attack='" + a.attack + "', n.defense='" + a.defense + "' return n",
            new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();
            int test = listBox1.SelectedIndex;
            listBox1.Items[test] = a.name;
        }
        
        private void modifySpellCard()
        {
            SpellCard a = spellCards[listBox1.SelectedIndex];
            a.name = textBoxName.Text;
            if (a.name == "")
                throw new Exception("Please enter card name!");
            a.description = textBoxDescription.Text;
            if (a.description == "")
                throw new Exception("Please enter card description!");
            a.condition = comboBoxCondition.SelectedItem.ToString();
            a.rarity = comboBoxRarity.SelectedItem.ToString();
            float m;
            if (float.TryParse(textBoxValue.Text, out m))
            {
                a.value = m;
                textBoxValue.BackColor = Color.White;
            }
            else
            {
                textBoxValue.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for value!");
            }
            a.icon = comboBoxSpellIcon.SelectedItem.ToString();

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + type + ") where n.id =~ " + a.id + " set n.name = '" + a.name
                                                                    + "', n.description='" + a.description + "', n.condition='" + a.condition
                                                                    + "', n.rarity='" + a.rarity + "', n.value='" + a.value
                                                                    + "', n.icon='" + a.icon + "' return n",
            new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();
            int test = listBox1.SelectedIndex;
            listBox1.Items[test] = a.name;
        }

        private void modifyTrapCard()
        {
            TrapCard a = trapCards[listBox1.SelectedIndex];
            a.name = textBoxName.Text;
            if (a.name == "")
                throw new Exception("Please enter card name!");
            a.description = textBoxDescription.Text;
            if (a.description == "")
                throw new Exception("Please enter card description!");
            a.condition = comboBoxCondition.SelectedItem.ToString();
            a.rarity = comboBoxRarity.SelectedItem.ToString();
            float m;
            if (float.TryParse(textBoxValue.Text, out m))
            {
                a.value = m;
                textBoxValue.BackColor = Color.White;
            }
            else
            {
                textBoxValue.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for value!");
            }
            a.icon = comboBoxTrapIcon.SelectedItem.ToString();

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + type + ") where n.id =~ " + a.id + " set n.name = '" + a.name
                                                                    + "', n.description='" + a.description + "', n.condition='" + a.condition
                                                                    + "', n.rarity='" + a.rarity + "', n.value='" + a.value
                                                                    +  "', n.icon='" + a.icon + "' return n",
            new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();
            int test = listBox1.SelectedIndex;
            listBox1.Items[test] = a.name;
        }

        private void buttonModifyCard_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedIndex < 0)
                    throw new Exception("Please select the card you wish to modify first!");
                if (type.Equals("MonsterCard"))
                    modifyMonsterCard();
                if (type.Equals("SpellCard"))
                    modifySpellCard();
                if(type.Equals("TrapCard"))
                    modifyTrapCard();
                MessageBox.Show("Successfully modified card!");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void buttonDeleteCard_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedIndex < 0)
                    throw new Exception("Please select the card you wish to delete first!");
                int id;
                if (type.Equals("MonsterCard"))
                {
                    id = monsterCards[listBox1.SelectedIndex].id;
                    monsterCards.RemoveAt(listBox1.SelectedIndex);
                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:"+type+") where n.id =~ "+id+" delete n",
                                                            new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();

                    comboBoxMonsterType.SelectedIndex = -1;
                    comboBoxMonsterAttribute.SelectedIndex = -1;
                    comboBoxMonsterLevel.SelectedIndex = -1;
                    textBoxMonsterAttack.Text = "";
                    textBoxMonsterDefense.Text = "";
                }
                if (type.Equals("SpellCard"))
                {
                    id = spellCards[listBox1.SelectedIndex].id;
                    spellCards.RemoveAt(listBox1.SelectedIndex);
                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + type + ") where n.id =~ " + id + " delete n",
                                                            new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<SpellCard>(query).ToList();

                    comboBoxSpellIcon.SelectedIndex = -1;
                }
                if (type.Equals("TrapCard"))
                {
                    id = trapCards[listBox1.SelectedIndex].id;
                    trapCards.RemoveAt(listBox1.SelectedIndex);
                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + type + ") where n.id =~ " + id + " delete n",
                                                            new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<TrapCard>(query).ToList();

                    comboBoxTrapIcon.SelectedIndex = -1;
                }

                textBoxName.Text = "";
                textBoxDescription.Text = "";
                comboBoxCondition.SelectedIndex = -1;
                comboBoxRarity.SelectedIndex = -1;
                textBoxValue.Text = "";

                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                MessageBox.Show("Successfully deleted card!");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

