﻿namespace TCG.Forms
{
    partial class ModifyClubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAcceptChanges = new System.Windows.Forms.Button();
            this.textBoxMonthlyFee = new System.Windows.Forms.TextBox();
            this.textBoxClubName = new System.Windows.Forms.TextBox();
            this.labelMonthlyFee = new System.Windows.Forms.Label();
            this.labelClubName = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.buttonChangeLeader = new System.Windows.Forms.Button();
            this.buttonDisbandClub = new System.Windows.Forms.Button();
            this.buttonBanUser = new System.Windows.Forms.Button();
            this.buttonUpdateClubName = new System.Windows.Forms.Button();
            this.buttonUpdateMonthlyFee = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFunds = new System.Windows.Forms.TextBox();
            this.buttonDeposit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMyFunds = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxDeposit = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonAcceptChanges
            // 
            this.buttonAcceptChanges.Location = new System.Drawing.Point(196, 318);
            this.buttonAcceptChanges.Name = "buttonAcceptChanges";
            this.buttonAcceptChanges.Size = new System.Drawing.Size(75, 41);
            this.buttonAcceptChanges.TabIndex = 11;
            this.buttonAcceptChanges.Text = "Done";
            this.buttonAcceptChanges.UseVisualStyleBackColor = true;
            this.buttonAcceptChanges.Click += new System.EventHandler(this.buttonAcceptChanges_Click);
            // 
            // textBoxMonthlyFee
            // 
            this.textBoxMonthlyFee.Location = new System.Drawing.Point(90, 89);
            this.textBoxMonthlyFee.Name = "textBoxMonthlyFee";
            this.textBoxMonthlyFee.Size = new System.Drawing.Size(100, 20);
            this.textBoxMonthlyFee.TabIndex = 10;
            // 
            // textBoxClubName
            // 
            this.textBoxClubName.Location = new System.Drawing.Point(90, 54);
            this.textBoxClubName.Name = "textBoxClubName";
            this.textBoxClubName.Size = new System.Drawing.Size(100, 20);
            this.textBoxClubName.TabIndex = 9;
            // 
            // labelMonthlyFee
            // 
            this.labelMonthlyFee.AutoSize = true;
            this.labelMonthlyFee.Location = new System.Drawing.Point(13, 92);
            this.labelMonthlyFee.Name = "labelMonthlyFee";
            this.labelMonthlyFee.Size = new System.Drawing.Size(68, 13);
            this.labelMonthlyFee.TabIndex = 8;
            this.labelMonthlyFee.Text = "Monthly Fee:";
            // 
            // labelClubName
            // 
            this.labelClubName.AutoSize = true;
            this.labelClubName.Location = new System.Drawing.Point(13, 57);
            this.labelClubName.Name = "labelClubName";
            this.labelClubName.Size = new System.Drawing.Size(60, 13);
            this.labelClubName.TabIndex = 7;
            this.labelClubName.Text = "Club name:";
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Location = new System.Drawing.Point(13, 26);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(184, 13);
            this.labelInfo.TabIndex = 6;
            this.labelInfo.Text = "Changing Club\'s name costs 20 EUR!";
            // 
            // buttonChangeLeader
            // 
            this.buttonChangeLeader.Location = new System.Drawing.Point(15, 263);
            this.buttonChangeLeader.Name = "buttonChangeLeader";
            this.buttonChangeLeader.Size = new System.Drawing.Size(75, 41);
            this.buttonChangeLeader.TabIndex = 12;
            this.buttonChangeLeader.Text = "Change Leader";
            this.buttonChangeLeader.UseVisualStyleBackColor = true;
            this.buttonChangeLeader.Click += new System.EventHandler(this.buttonChangeLeader_Click);
            // 
            // buttonDisbandClub
            // 
            this.buttonDisbandClub.Location = new System.Drawing.Point(105, 263);
            this.buttonDisbandClub.Name = "buttonDisbandClub";
            this.buttonDisbandClub.Size = new System.Drawing.Size(75, 41);
            this.buttonDisbandClub.TabIndex = 13;
            this.buttonDisbandClub.Text = "Disband Club";
            this.buttonDisbandClub.UseVisualStyleBackColor = true;
            this.buttonDisbandClub.Click += new System.EventHandler(this.buttonDisbandClub_Click);
            // 
            // buttonBanUser
            // 
            this.buttonBanUser.Location = new System.Drawing.Point(196, 263);
            this.buttonBanUser.Name = "buttonBanUser";
            this.buttonBanUser.Size = new System.Drawing.Size(75, 41);
            this.buttonBanUser.TabIndex = 14;
            this.buttonBanUser.Text = "Kick User from Club";
            this.buttonBanUser.UseVisualStyleBackColor = true;
            this.buttonBanUser.Click += new System.EventHandler(this.buttonBanUser_Click);
            // 
            // buttonUpdateClubName
            // 
            this.buttonUpdateClubName.Location = new System.Drawing.Point(197, 54);
            this.buttonUpdateClubName.Name = "buttonUpdateClubName";
            this.buttonUpdateClubName.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateClubName.TabIndex = 15;
            this.buttonUpdateClubName.Text = "Update";
            this.buttonUpdateClubName.UseVisualStyleBackColor = true;
            this.buttonUpdateClubName.Click += new System.EventHandler(this.buttonUpdateClubName_Click);
            // 
            // buttonUpdateMonthlyFee
            // 
            this.buttonUpdateMonthlyFee.Location = new System.Drawing.Point(197, 89);
            this.buttonUpdateMonthlyFee.Name = "buttonUpdateMonthlyFee";
            this.buttonUpdateMonthlyFee.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateMonthlyFee.TabIndex = 16;
            this.buttonUpdateMonthlyFee.Text = "Update";
            this.buttonUpdateMonthlyFee.UseVisualStyleBackColor = true;
            this.buttonUpdateMonthlyFee.Click += new System.EventHandler(this.buttonUpdateMonthlyFee_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Club Funds:";
            // 
            // textBoxFunds
            // 
            this.textBoxFunds.Enabled = false;
            this.textBoxFunds.Location = new System.Drawing.Point(90, 122);
            this.textBoxFunds.Name = "textBoxFunds";
            this.textBoxFunds.Size = new System.Drawing.Size(100, 20);
            this.textBoxFunds.TabIndex = 18;
            // 
            // buttonDeposit
            // 
            this.buttonDeposit.Location = new System.Drawing.Point(197, 221);
            this.buttonDeposit.Name = "buttonDeposit";
            this.buttonDeposit.Size = new System.Drawing.Size(75, 23);
            this.buttonDeposit.TabIndex = 19;
            this.buttonDeposit.Text = "Deposit";
            this.buttonDeposit.UseVisualStyleBackColor = true;
            this.buttonDeposit.Click += new System.EventHandler(this.buttonDeposit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "My Funds:";
            // 
            // textBoxMyFunds
            // 
            this.textBoxMyFunds.Enabled = false;
            this.textBoxMyFunds.Location = new System.Drawing.Point(90, 155);
            this.textBoxMyFunds.Name = "textBoxMyFunds";
            this.textBoxMyFunds.Size = new System.Drawing.Size(100, 20);
            this.textBoxMyFunds.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Amount to Deposit from My to Club Funds:";
            // 
            // textBoxDeposit
            // 
            this.textBoxDeposit.Location = new System.Drawing.Point(90, 223);
            this.textBoxDeposit.Name = "textBoxDeposit";
            this.textBoxDeposit.Size = new System.Drawing.Size(100, 20);
            this.textBoxDeposit.TabIndex = 23;
            // 
            // ModifyClubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 376);
            this.Controls.Add(this.textBoxDeposit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxMyFunds);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonDeposit);
            this.Controls.Add(this.textBoxFunds);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonUpdateMonthlyFee);
            this.Controls.Add(this.buttonUpdateClubName);
            this.Controls.Add(this.buttonBanUser);
            this.Controls.Add(this.buttonDisbandClub);
            this.Controls.Add(this.buttonChangeLeader);
            this.Controls.Add(this.buttonAcceptChanges);
            this.Controls.Add(this.textBoxMonthlyFee);
            this.Controls.Add(this.textBoxClubName);
            this.Controls.Add(this.labelMonthlyFee);
            this.Controls.Add(this.labelClubName);
            this.Controls.Add(this.labelInfo);
            this.Name = "ModifyClubForm";
            this.Text = "ModifyClubForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAcceptChanges;
        private System.Windows.Forms.TextBox textBoxMonthlyFee;
        private System.Windows.Forms.TextBox textBoxClubName;
        private System.Windows.Forms.Label labelMonthlyFee;
        private System.Windows.Forms.Label labelClubName;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Button buttonChangeLeader;
        private System.Windows.Forms.Button buttonDisbandClub;
        private System.Windows.Forms.Button buttonBanUser;
        private System.Windows.Forms.Button buttonUpdateClubName;
        private System.Windows.Forms.Button buttonUpdateMonthlyFee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFunds;
        private System.Windows.Forms.Button buttonDeposit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMyFunds;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxDeposit;
    }
}