﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class ModifyClubForm : Form
    {
        private GraphClient client;
        private User user;
        private Club club;

        public ModifyClubForm()
        {
            InitializeComponent();
        }
        public ModifyClubForm(GraphClient cl, User us)
        {
            client = cl;
            user = us;
            club = user.club;
            InitializeComponent();
            textBoxClubName.Text = club.name;
            textBoxMonthlyFee.Text = club.monthlyFee.ToString();
            textBoxFunds.Text = club.funds.ToString();
            textBoxMyFunds.Text = user.funds.ToString();
        }

        private void buttonUpdateClubName_Click(object sender, EventArgs e)
        {
            if (club.funds > 20)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.name =~ '" + textBoxClubName.Text
                                                                       + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                List<Club> clubs = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList();

                if (clubs.Count == 0)
                {
                    float temp = club.funds - 20;
                    var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id =~ '" + club.id + "' set n.name = '" + textBoxClubName.Text
                                                                   + "', n.funds = '" + temp
                                                                   + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                    ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query1).ToList();

                    club.name = textBoxClubName.Text;
                    club.funds = temp;
                    textBoxFunds.Text = club.funds.ToString();
                    MessageBox.Show("Club's name is changed!");

                }
                else
                {
                    MessageBox.Show("Club with that name already exists!"); //ili postoji klub sa istim imenom ili je ime nepromenjeno
                    textBoxClubName.Text = club.name;
                }
            }
            else
            {
                MessageBox.Show("Insufficient funds!");
                textBoxClubName.Text = club.name;
            }
        }

        private void buttonUpdateMonthlyFee_Click(object sender, EventArgs e)
        {
            float n;
            try
            {
                if (float.TryParse(textBoxMonthlyFee.Text, out n))
                {
                    if (n != club.monthlyFee)
                    {
                        var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id =~ '" + club.id + "' set n.monthlyFee = '" + n
                                                                            + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query1).ToList();
                        club.monthlyFee = n;
                        MessageBox.Show("Monthly fee changed!");
                    }
                }
                else
                {
                    throw new Exception("Please enter only numbers for monthly fee!");
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonDeposit_Click(object sender, EventArgs e)
        {
            float n;
            try
            {
                if (float.TryParse(textBoxDeposit.Text, out n))
                {
                    if (n <= user.funds)
                    {
                        float m = n+club.funds;
                        var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Club) where n.id =~ '" + club.id + "' set n.funds = '" + m
                                                                            + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query1).ToList();
                        club.funds = m;
                        textBoxFunds.Text = m.ToString();

                        m = user.funds - n;
                        var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id =~ '" + user.id + "' set n.funds = '" + m
                                                                            + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query2).ToList();
                        user.funds = m;
                        textBoxMyFunds.Text = m.ToString();
                        
                        MessageBox.Show("Money successfully deposited!");
                    }
                    else
                        MessageBox.Show("Insufficient funds!");
                }
                else
                {
                    throw new Exception("Please enter only numbers for deposit!");
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonChangeLeader_Click(object sender, EventArgs e)
        {
            
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Club),(m:User) where m-[:MEMBER]->n and n.id =~ '" +
                                                          club.id + "' return m",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);

            List<User> users = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();
            users.RemoveAt(users.FindIndex(use => use.id.Equals(user.id, StringComparison.Ordinal)));
            if (users.Count > 0)
            {
                ChangeLeaderForm change = new ChangeLeaderForm(client, user, club, users);
                change.ShowDialog();//ispitati da li je i dalje leader
                if (!user.isLeader)
                    Close();
            }
            else
                MessageBox.Show("You are the only member!");
        }

        private void buttonBanUser_Click(object sender, EventArgs e)
        {
            KickUserForm kick = new KickUserForm(user, client);
            kick.ShowDialog();
        }

        private void buttonAcceptChanges_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonDisbandClub_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Club),(m:User) where m-[:MEMBER]->n and n.id =~ '" +
                                                          club.id + "' return m",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);

            List<User> users = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();
            //users.RemoveAt(users.FindIndex(use => use.id.Equals(user.id, StringComparison.Ordinal)));

            if (club.funds >= club.monthlyFee * (users.Count - 1))
            {
                foreach (User u in users)
                {
                    var query1 = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User), b-[r: MEMBER]->a where b.id = '" + u.id
                                                            + "' AND a.id = '" + club.id + "' delete r",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
                    ((IRawGraphClient)client).ExecuteCypher(query1);

                    if (u.id != user.id )
                    {
                        u.funds += user.club.monthlyFee;
                        var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id =~ '" + u.id
                                                                        + "' set n.funds = '" + u.funds
                                                                        + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteCypher(query2);
                    }
                    else//leader
                    {
                        var query3 = new Neo4jClient.Cypher.CypherQuery("match (m:User) where m.id =~ '" + user.id
                                                          + "' set m.isLeader = 'false' return m",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);
                        ((IRawGraphClient)client).ExecuteCypher(query3);
                        user.isLeader = false;
                        user.club = null;
                    }
                }

                var query4 = new Neo4jClient.Cypher.CypherQuery("match (m:Club) where m.id =~ '" + club.id + "' delete m",
                                                        new Dictionary<string, object>(), CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query4);

                MessageBox.Show("You have successfully disbanded your Club!");
                Close();
            }
            else
                MessageBox.Show("Insufficient funds, you need to repay one monthly fee to each member before disbanding the club!");
        }
    }
}
