﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class MyCollectionForm : Form
    {
        private User user;
        private GraphClient client;

        public MyCollectionForm(User u, GraphClient cl)
        {
            InitializeComponent();
            user = u;
            client = cl;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (comboBoxType.SelectedItem.Equals("MonsterCard"))
            {
                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                String queryText = "match (a:MonsterCard), (b:User), b-[r:OWNS]->a where b.id = '" + user.id +"'";
                commonQueryProperties(queryText);
                if (comboBoxMonsterType.SelectedIndex != -1)
                    queryText += " and a.type = '" + comboBoxMonsterType.SelectedItem.ToString() + "'";
                if (comboBoxMonsterAttribute.SelectedIndex != -1)
                    queryText += " and a.attribute = '" + comboBoxMonsterAttribute.SelectedItem.ToString() + "'";
                if (comboBoxMonsterLevel.SelectedIndex != -1)
                    queryText += " and a.type = '" + comboBoxMonsterLevel.SelectedItem.ToString() + "'";
                int m;
                if (int.TryParse(textBoxFromAttack.Text, out m))
                    queryText += " and a.attack >= '" + m + "'";
                if (int.TryParse(textBoxToAttack.Text, out m))
                    queryText += " and a.attack <= '" + m + "'";
                if (int.TryParse(textBoxFromDefense.Text, out m))
                    queryText += " and a.defense >= '" + m + "'";
                if (int.TryParse(textBoxToDefense.Text, out m))
                    queryText += " and a.defense <= '" + m + "'";

                queryText += " return a";

                var query = new Neo4jClient.Cypher.CypherQuery(queryText, queryDict, CypherResultMode.Set);

                List<MonsterCard> monsterCards = ((IRawGraphClient)client).ExecuteGetCypherResults<MonsterCard>(query).ToList();

                foreach (MonsterCard mC in monsterCards)
                {
                    listBox1.Items.Add(mC.name);
                }
            }

        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxType.SelectedItem.Equals("MonsterCard"))
            {
                groupBoxMonsterCard.Visible = true;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("SpellCard"))
            {
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = true;
                groupBoxTrapCard.Visible = false;
            }
            if (comboBoxType.SelectedItem.Equals("TrapCard"))
            {
                groupBoxMonsterCard.Visible = false;
                groupBoxSpellCard.Visible = false;
                groupBoxTrapCard.Visible = true;
            }
        }

        private void commonQueryProperties(string queryText)
        {
            float m;
            if (textBoxName.Text != "")
                queryText += " and a.name = '" + textBoxName.Text + "'";
            if (comboBoxCondition.SelectedIndex != -1)
                queryText += " and a.condition = '" + comboBoxCondition.SelectedItem.ToString() + "'";
            if (comboBoxRarity.SelectedIndex != -1)
                queryText += " and a.rarity = '" + comboBoxRarity.SelectedItem.ToString() + "'";

            if (float.TryParse(textBoxFromValue.Text, out m))
                queryText += " and a.value >= '" + m + "'";
            if (float.TryParse(textBoxToValue.Text, out m))
                queryText += " and a.value <= '" + m + "'";
        }
    }
}
