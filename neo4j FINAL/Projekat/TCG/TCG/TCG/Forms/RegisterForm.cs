﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using TCG.DomainModel;

namespace TCG.Forms
{
    public partial class RegisterForm : Form
    {
        public GraphClient client;
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            User user = createUser();           //add provera da ne postoji vec takav
            string maxId = getMaxId("User");

            try
            {
                int mId = int.Parse(maxId);
                user.id = (++mId).ToString();
            }
            catch (Exception exception)
            {
                user.id = "1";
            }


            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("username", user.username);
            queryDict.Add("password", user.password);
            queryDict.Add("firstName", user.firstName);
            queryDict.Add("lastName", user.lastName);
            queryDict.Add("isLeader", user.isLeader);
            queryDict.Add("funds", user.funds);
            queryDict.Add("__type__", "org.neo4j.TCG.domain.User");

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:User {id:'" + user.id + "', username:'" + user.username
                                                            + "', password:'" + user.password + "', firstName:'" + user.firstName
                                                            + "', lastName:'" + user.lastName + "', isLeader:'" + user.isLeader
                                                            + "', funds:'" + user.funds
                                                            + "', __type__:'org.neo4j.TCG.domain.User'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<User> users = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();
            MessageBox.Show("User " + user.username + " created!");
            Close();
        }



        private User createUser()
        {
            User a = new User();

            a.funds = 1000; //promeniti kasnije, ne zaboravi za start club
            a.username = usernameTextBox.Text;
            a.password = passwordTextBox.Text;
            a.firstName = firstnameTextBox.Text;
            a.lastName = lastnameTextBox.Text;
            a.isLeader = false;

            return a;
        }

        private String getMaxId(string s)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + s + ") where has(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
