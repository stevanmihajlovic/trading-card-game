﻿namespace TCG.Forms
{
    partial class StartAClubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelClubName = new System.Windows.Forms.Label();
            this.labelMonthlyFee = new System.Windows.Forms.Label();
            this.textBoxClubName = new System.Windows.Forms.TextBox();
            this.textBoxMonthlyFee = new System.Windows.Forms.TextBox();
            this.buttonStartAClub = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Location = new System.Drawing.Point(26, 23);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(154, 13);
            this.labelInfo.TabIndex = 0;
            this.labelInfo.Text = "Starting a Club costs 100 EUR!";
            // 
            // labelClubName
            // 
            this.labelClubName.AutoSize = true;
            this.labelClubName.Location = new System.Drawing.Point(12, 57);
            this.labelClubName.Name = "labelClubName";
            this.labelClubName.Size = new System.Drawing.Size(60, 13);
            this.labelClubName.TabIndex = 1;
            this.labelClubName.Text = "Club name:";
            // 
            // labelMonthlyFee
            // 
            this.labelMonthlyFee.AutoSize = true;
            this.labelMonthlyFee.Location = new System.Drawing.Point(12, 97);
            this.labelMonthlyFee.Name = "labelMonthlyFee";
            this.labelMonthlyFee.Size = new System.Drawing.Size(68, 13);
            this.labelMonthlyFee.TabIndex = 2;
            this.labelMonthlyFee.Text = "Monthly Fee:";
            // 
            // textBoxClubName
            // 
            this.textBoxClubName.Location = new System.Drawing.Point(89, 54);
            this.textBoxClubName.Name = "textBoxClubName";
            this.textBoxClubName.Size = new System.Drawing.Size(100, 20);
            this.textBoxClubName.TabIndex = 3;
            // 
            // textBoxMonthlyFee
            // 
            this.textBoxMonthlyFee.Location = new System.Drawing.Point(89, 94);
            this.textBoxMonthlyFee.Name = "textBoxMonthlyFee";
            this.textBoxMonthlyFee.Size = new System.Drawing.Size(100, 20);
            this.textBoxMonthlyFee.TabIndex = 4;
            // 
            // buttonStartAClub
            // 
            this.buttonStartAClub.Location = new System.Drawing.Point(195, 133);
            this.buttonStartAClub.Name = "buttonStartAClub";
            this.buttonStartAClub.Size = new System.Drawing.Size(75, 23);
            this.buttonStartAClub.TabIndex = 5;
            this.buttonStartAClub.Text = "Start A Club";
            this.buttonStartAClub.UseVisualStyleBackColor = true;
            this.buttonStartAClub.Click += new System.EventHandler(this.buttonStartAClub_Click);
            // 
            // StartAClubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 392);
            this.Controls.Add(this.buttonStartAClub);
            this.Controls.Add(this.textBoxMonthlyFee);
            this.Controls.Add(this.textBoxClubName);
            this.Controls.Add(this.labelMonthlyFee);
            this.Controls.Add(this.labelClubName);
            this.Controls.Add(this.labelInfo);
            this.Name = "StartAClubForm";
            this.Text = "StartAClubForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Label labelClubName;
        private System.Windows.Forms.Label labelMonthlyFee;
        private System.Windows.Forms.TextBox textBoxClubName;
        private System.Windows.Forms.TextBox textBoxMonthlyFee;
        private System.Windows.Forms.Button buttonStartAClub;
    }
}