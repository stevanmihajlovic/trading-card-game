﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4jClient;
using TCG.DomainModel;
using Neo4jClient.Cypher;

namespace TCG.Forms
{
    public partial class StartAClubForm : Form
    {
        private GraphClient client;
        private User user;
        private Club club;


        public StartAClubForm()
        {
            
        }

        public StartAClubForm(GraphClient cl, User us)
        {
            client = cl;
            user = us;
            InitializeComponent();
        }

        private void buttonStartAClub_Click(object sender, EventArgs e)
        {
            if (user.funds >= 100)
            {
                List<Club> cl = new List<Club>();

                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Club) WHERE n.name =~ '" + textBoxClubName.Text + "' return n;",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);
                cl = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList();

                if (cl.Count == 1)
                    MessageBox.Show("Club with this name already exists!");
                else
                {
                    try
                    {
                        club = createClub();

                        string maxId = getMaxId("Club");

                        try
                        {
                            int mId = int.Parse(maxId);
                            club.id = (++mId).ToString();
                        }
                        catch (Exception exception)
                        {
                            club.id = "1";
                        }
                        Dictionary<string, object> queryDict = new Dictionary<string, object>();
                        queryDict.Add("name", club.name);
                        queryDict.Add("monthlyFee", club.monthlyFee);
                        queryDict.Add("discount", club.discount);
                        queryDict.Add("funds", club.funds);
                        //queryDict.Add("members", club.members);
                        //queryDict.Add("leader", club.leader.username);
                        queryDict.Add("__type__", "org.neo4j.TCG.domain.Club");

                        var query1 = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Club {id:'" + club.id + "', name:'" + club.name
                                                                        + "', monthlyFee:'" + club.monthlyFee
                                                                        + "', discount:'" + club.discount + "', funds:'" + club.funds
                                                                        //+ "', members:'" + club.members + "',
                                                                        //+ "'leader:'" + club.leader.username
                                                                        + "', __type__:'org.neo4j.TCG.domain.Club'}) return n",
                                                                        queryDict, CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query1).ToList();

                        /* var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(a: User),(b:Club) WHERE a.username =~ '" + user.username
                                                                         + "' AND b.name =~ '" + club.name + "' " +
                                                                         "CREATE(a) -[r: LEADS]->(b) RETURN r", new Dictionary<string, object>(),
                                                                         CypherResultMode.Set);

                         ((IRawGraphClient)client).ExecuteGetCypherResults<RelLeads>(query2).ToList();*/

                        var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH(a: User),(b:Club) WHERE a.username =~ '" + user.username
                                                                        + "' AND b.name =~ '" + club.name + "' " +
                                                                        "CREATE(a) -[r: MEMBER{ isLeader : 'true' }]->(b) RETURN r",
                                                                        new Dictionary<string, object>(),
                                                                        CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<RelMember>(query3).ToList();

                        float m = user.funds - 100;
                        user.isLeader = true;
                        var query2 = new Neo4jClient.Cypher.CypherQuery("match (n:User) where n.id =~ '" + user.id + "' set n.funds = '" + m
                                                                        + "', n.isLeader = '" + user.isLeader
                                                                        + "' return n", new Dictionary<string, object>(), CypherResultMode.Set);

                        ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query2).ToList();
                        user.funds = m;
                        user.club = club;
                        //user.isLeader = true;
                        MessageBox.Show("Club " + club.name + " created!");

                        Close();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }
            }
            else
                MessageBox.Show("Insufficient funds!");
        }

        private Club createClub()
        {
            Club a = new Club();

            a.name = textBoxClubName.Text;
            float n;
            if (float.TryParse(textBoxMonthlyFee.Text, out n))
                a.monthlyFee = n;
            else
            {
                textBoxMonthlyFee.BackColor = Color.Red;
                throw new Exception("Please enter only numbers for monthly fee!");
            }
            a.members.Add(user);
            a.leader = user;
            a.discount = 0;
            a.funds = 50;//50 goes to store

            return a;
        }

        private String getMaxId(string s)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + s + ") where has(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
