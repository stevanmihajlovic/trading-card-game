﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace TCG.Forms
{
    public partial class StartForm : Form
    {
        public GraphClient client;
        public StartForm()
        {
            InitializeComponent();
        }        
       
        private void registerButton_Click(object sender, EventArgs e)
        {
            RegisterForm reg = new RegisterForm();
            reg.client = this.client;
            reg.ShowDialog();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "12345");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            LogInForm form = new LogInForm();
            form.client = client;
            form.ShowDialog();
        }

        private void buttonAdminAccess_Click(object sender, EventArgs e)
        {
            AdminAccessForm reg = new AdminAccessForm();
            reg.client = this.client;
            reg.ShowDialog();
        }
    }
}
