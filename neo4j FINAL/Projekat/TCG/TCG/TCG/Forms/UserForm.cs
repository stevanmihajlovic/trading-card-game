﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCG.DomainModel;


namespace TCG.Forms
{
    public partial class UserForm : Form
    {
        User user;
        public GraphClient client;

        public UserForm()
        {
            InitializeComponent();
        }
        public UserForm (User u, GraphClient cl)
        {
            InitializeComponent();

            client = cl;
            user = u;
            usernameLabel.Text = u.username;
            firstnameLabel.Text = u.firstName;
            lastnameLabel.Text = u.lastName;

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:User), (c:Club), n-[MEMBER]->c where n.id =~ '" + user.id
                                                                          + "' return c", new Dictionary<string, object>(), CypherResultMode.Set);
            
            u.club = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList().FirstOrDefault();

            if (u.club == null)
                clubLabel.Text = "Not a member.";
            else
                clubLabel.Text = u.club.name;

            updateInterface();

        }

    private void buttonJoinCLub_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Club) return n;",
                                              new Dictionary<string, object>(), CypherResultMode.Set);
            List<Club> clubs = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList();

            if (clubs.Count == 0)
                MessageBox.Show("There are no Clubs to join!");
            else
            {
                JoinAClubForm join = new JoinAClubForm(client, clubs, user);
                join.ShowDialog();
                updateInterface();
            }

        }

        private void buttonStartClub_Click(object sender, EventArgs e)
        {
            StartAClubForm start = new StartAClubForm(client, user);
            start.ShowDialog();
            updateInterface();
        }

        private void buttonModifyClub_Click(object sender, EventArgs e)
        {
            /*var query = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User),b-[r:MEMBER]->a where r.isLeader =~ 'true' "
                                                            + "and b.username =~ '"+user.username+"' return a",
                                              new Dictionary<string, object>(), CypherResultMode.Set);

            List<Club> cl = ((IRawGraphClient)client).ExecuteGetCypherResults<Club>(query).ToList();*/
            ModifyClubForm modify = new ModifyClubForm(client, user);
            modify.ShowDialog();
            updateInterface();
        }

        private void updateInterface()
        {
            fundsLabel.Text = user.funds.ToString();
            if (user.isLeader)
            {
                clubLabel.Text = user.club.name;
                buttonModifyClub.Visible = true;
                buttonLeaveClub.Visible = false;
                buttonJoinCLub.Visible = false;
                buttonStartClub.Visible = false;
            }
            else
                if (user.club != null)
                {
                    clubLabel.Text = user.club.name;
                    buttonModifyClub.Visible = false;
                    buttonLeaveClub.Visible = true;
                    buttonJoinCLub.Visible = false;
                    buttonStartClub.Visible = false;
                }
                else
                {
                    clubLabel.Text = "Not a member.";
                    buttonModifyClub.Visible = false;
                    buttonLeaveClub.Visible = false;
                    buttonJoinCLub.Visible = true;
                    buttonStartClub.Visible = true;
                }
            
            
        }

        private void buttonLeaveClub_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (a:Club),(b:User), b-[r: MEMBER]->a where b.id = '" + user.id
                                                            + "' AND a.id = '" + user.club.id + "' delete r",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);

            user.club = null;

            updateInterface();
        }

        private void buttonMyCollection_Click(object sender, EventArgs e)
        {
            MyCollectionForm myCollection = new MyCollectionForm(user, client);
            myCollection.ShowDialog();
        }

        private void buttonAddCardToMyCollection_Click(object sender, EventArgs e)
        {
            AddCardsToCollectionForm addToCollection = new AddCardsToCollectionForm(user, client);
            addToCollection.ShowDialog();
        }
    }
}
